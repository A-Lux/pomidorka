<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage'=>'ru',
    'language'=>'ru',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
            ]
        ],
        'request' => [
            'baseUrl' => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'loginUrl' => '/login',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
        ],
        'urlManager' => [
            'class'                         => codemix\localeurls\UrlManager::className(),
            'languages'                     => ['ru'],
            'enableDefaultLanguageUrlCode'  => false,
            'enablePrettyUrl'               => true,
            'showScriptName'                => false,
            'rules' => [
                '/'                                 => '/site/index',
                'catalog/<slug:[\w_\/-]+>'          => 'catalog/view',

                'stocks/<id:[\w_\/-]+>'             => 'stocks/view',
                'profile/<action:\w+>'              => 'profile/index',
                'authentication/<action:\w+>'       => 'authentication/index',
            ],
        ],
    ],
    'params' => $params,
];
