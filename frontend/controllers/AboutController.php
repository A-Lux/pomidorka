<?php

namespace frontend\controllers;

use common\models\About;
use common\models\Menu;

class AboutController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('about');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $model  = About::getOne();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}