<?php

namespace frontend\controllers;

use common\models\Address;
use common\models\Contacts;
use common\models\Faq;
use common\models\Menu;

class ContactController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('contact');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $address    = Address::getAllAddress();
        $contact    = Contacts::getOne();
        $faq        = Faq::getActiveQuestion();

        return $this->render('index', [
            'address'   => $address,
            'contact'   => $contact,
            'faq'       => $faq,
        ]);
    }
}