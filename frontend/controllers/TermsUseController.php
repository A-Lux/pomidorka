<?php

namespace frontend\controllers;

use common\models\Menu;
use common\models\TermsUse;

class TermsUseController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('terms_use_footer');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $model  = TermsUse::getOne();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}