<?php

namespace frontend\controllers;

use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class ProfileController extends FrontendController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

}