<?php

/* @var $this yii\web\View */
/* @var $model PrivacyPolicy */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\PrivacyPolicy;

?>
<div class="static-page">
    <div class="container py-5">
        <div class="row mb-4">
            <div class="col-lg-12">
                <h1><?= $model->title; ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <img src="<?= $model->getImage(); ?>" alt="">
            </div>
            <div class="col-lg-8 col-md-6">
               <?= $model->content; ?>
            </div>
        </div>
    </div>
</div>