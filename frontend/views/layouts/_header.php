<?php

use yii\helpers\Url;
use common\models\Catalog;

$catalog    = \Yii::$app->view->params['categories'];
$menuHeader = Yii::$app->view->params['menuHeader'];

?>
<div class="header">
    <app-link-component :appsource="`<?= \Yii::$app->view->params['contact']->app_store; ?>`" :androidsource="`<?= \Yii::$app->view->params['contact']->play_market ?>`">
    </app-link-component>
    <div class="border-bottom">
        <div class="container">
            <div class="row py-2">
                <div class="col-lg-2 col-md-2 col-sm-3 col-4 d-flex align-items-center">
                    <div class="location">
                        <i class="fas fa-map-marker-alt"></i>
                        <p>Алматы</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-5 col-8">
                    <nav class="main-nav">
                        <ul>
                            <? foreach ($menuHeader as $menu): ?>
                            <li>
                                <a href="<?= $menu->url; ?>">
                                    <?= $menu->name; ?>
                                </a>
                            </li>
                            <? endforeach; ?>
                        </ul>
                    </nav>
                </div>
                <div class="offset-xl-4 offset-lg-3 offset-md-1"></div>
                <div class="col-xl-3 col-lg-4 col-md-5 col-sm-4">
                    <ul class="apps">
                        <li>
                            <a href="<?= \Yii::$app->view->params['contact']->play_market ?>" target="_blank">
                                <img src="/images/googleplay.png">
                            </a>
                        </li>
                        <li>
                            <a href="<?= \Yii::$app->view->params['contact']->app_store ?>" target="_blank">
                                <img src="/images/appstore.png">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-2">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3 col-4">
                <a href="/" style="display: contents;">
                    <img class="logotype" src="<?= \Yii::$app->view->params['logoHeader']->getImage(); ?>">
                </a>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-8 d-flex align-items-center">
                <p class="title">
                    <?= Yii::t('main-header', 'Доставляем удовольствие каждому клиенту'); ?>
                </p>
            </div>
            <div class="offset-lg-4">

            </div>
            <div class="col-lg-2 col-md-3 col-sm-4 col-6 d-flex align-items-center">
                <div class="contact-wall">
                    <a href="tel:<?= \Yii::$app->view->params['contact']->phone; ?>">
                        <?= \Yii::$app->view->params['contact']->phone; ?>
                    </a>
                    <span>
                        <?= Yii::t('main-header', 'Звоните нам'); ?>
                    </span>
                </div>
            </div>
            <div class="offset-sm-0 offset-2"></div>
            <div class="col-lg-1 col-md-2 col-sm-1 col-4">
                <cart-link-component></cart-link-component>
            </div>
        </div>
    </div>
    <div class="border-top">
        <div class="container">
            <div class="row py-4 py-md-0">
                <div class="col-lg-9 col-md-9 col-6">
                    <a class="toggle" href="#">
                        <span></span>
                    </a>
                    <nav class="categories">
                        <ul>
                            <? foreach ($catalog as $category): ?>
                            <li>
                                <a class="" href="<?= Url::to(['/catalog/view', 'slug' => $category->slug]) ?>">
                                    <?= $category->name; ?>
                                </a>
                            </li>
                            <? endforeach; ?>

                        </ul>
                    </nav>
                </div>
                <div class="offset-lg-1"></div>
                <div class="col-lg-2 col-md-3 col-6 d-flex align-items-center">
                    <auth-link-component></auth-link-component>
                </div>
            </div>
        </div>
    </div>
</div>