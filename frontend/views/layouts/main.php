<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\ToastrAlert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="/css/main.css">
    <?php $this->head() ?>
    <link rel="icon" type="image/png" href="/images/favicon.png">

    <!--    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>-->
    <!--    <script>-->
    <!--        window.OneSignal = window.OneSignal || [];-->
    <!--        OneSignal.push(function() {-->
    <!--            OneSignal.init({-->
    <!--                appId: "2d92cfc0-0f04-434d-8879-28b831e1a1a0",-->
    <!--            });-->
    <!--        });-->
    <!--    </script>-->
</head>

<body>
    <?php $this->beginBody() ?>
    <div id="app">
        <!-- HEADER -->
        <?= $this->render('_header') ?>
        <!-- END HEADER -->

        <?= \yii2mod\alert\Alert::widget() ?>
        <!-- CONTENT -->
        <?= $content ?>
        <!-- END CONTENT -->

        <!-- FOOTER -->
        <?= $this->render('_footer') ?>
        <!-- END FOOTER -->
    </div>
    <script src="/js/app.js"></script>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=8350983e-1e36-4ac2-95ed-62859584bf6f&lang=ru_RU"></script>
    <script src="/js/main.js"></script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>