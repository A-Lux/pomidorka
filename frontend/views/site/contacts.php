<div class="contacts">
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-12">
                <h1>Контакты</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="map"></div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-lg-4">
                <div class="info phone">
                    <a href="tel:8 800 300-00-60">8 800 300-00-60</a>
                    <p>
                        <?= Yii::t('main-contact','Сделайте заказ по телефону'); ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="info mail">
                    <a href="mailto:info@mail.ru">info@mail.ru</a>
                    <p>
                        <?= Yii::t('main-contact','Вопросы, отзывы и предложения по улучшению сервиса и качества'); ?>
                    </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="info address">
                    <a href="javascript:void(0)">Алматы, ул. Красная 19</a>
                    <p>
                        <?= Yii::t('main-contact','Приезжайте к нам'); ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 my-5">
                <h2>
                    <?= Yii::t('main-contact','Часто задавемые вопросы:'); ?>
                </h2>
            </div>
            <div class="col-lg-12">
                <div class="accordion" id="accordion">
                    <div class="card">
                        <div class="card-header" id="heading1" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            <span>
                                Что делать, если заказ не привезли?
                            </span>
                            <i class="fal fa-plus"></i>
                        </div>
                        <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordion">
                            <div class="card-body">
                                ...
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading2" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
                            <span>
                                Существуют ли скидки на день рождения?
                            </span>
                            <i class="fal fa-plus"></i>
                        </div>
                        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
                            <div class="card-body">
                                ...
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="heading3" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
                            <span>
                                Как отписаться от рассылок?
                            </span>
                            <i class="fal fa-plus"></i>
                        </div>
                        <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
                            <div class="card-body">
                                ...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 my-5">
                <button class="ask-btn">
                    <?= Yii::t('main-contact','Задать свой вопрос'); ?>
                </button>
            </div>
        </div>
    </div>
</div>