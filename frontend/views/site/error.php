<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title            = $name;
$this->context->layout  = '@frontend/views/layouts/error';
?>
<div class="all-wrapper error-page">

    <div class="error-title"><?= $name ?></div>
    <h3 class="bold-text"><?= $exception->getMessage();?></h3>
    <p class="common-text">
        <?= Yii::t('main-phrase', 'Для возврата на главную страницу перейдите по {link}', [
            'link' => Html::a(Yii::t('main-link', 'ссылке'), Url::home()),
        ]) ?>
    </p>
</div>

