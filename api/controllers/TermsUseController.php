<?php

namespace api\controllers;

use common\models\TermsUse;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class TermsUseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return TermsUse::getOne();

    }
}