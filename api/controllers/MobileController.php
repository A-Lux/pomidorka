<?php

namespace api\controllers;

use api\models\OrderMobileForm;
use api\models\UserForm;
use common\models\Orders;
use common\models\User;
use yii\db\Exception;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class MobileController extends Controller
{
    public function actionUpdate()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        $model = new UserForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->updateUser()) {
                $data   = $model->dataUser($user);

                $response   = [
                    'message'   => \Yii::t('main-message', 'Вы успешно обновили личные данные!'),
                    'user'      => $data,
                ];
                return $response;

            } else {
                $errors = $model->firstErrors;
                \Yii::$app->response->setStatusCode(422);
                foreach ($errors as $error) {
                    $message   = $error;
                    break;
                }
                $response   = ['status' => 0, 'message' => $message];
                return $response;
            }
        }catch (\Exception $e){
            throw new HttpException(403, 'Ошибка сервера! Попробуйте позднее!');
        }
    }

    public function actionOrderCreate()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        $model  = new OrderMobileForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(), '')){

            try {
                if ($order = $model->create()) {
                    if($order->typePay == 4){
                        $response   = [
                            'status'    => 1,
                            'message'   => 'Благодарим Вас за покупку!',
                            'order_id'  => $order->id,
                            'url'       => '/api/pay?id=' . $order->id,
                        ];
                    }else {
                        $response = [
                            'status' => 1,
                            'message' => 'Благодарим Вас за покупку!',
                            'order_id' => $order->id
                        ];
                    }

                    return $response;

                } else {
                    $errors = $model->firstErrors;
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                throw new HttpException(403, $e->getMessage());
            }

        }
    }

    /**
     * view all orders.
     * @param  $token
     * @return mixed
     * @throws
     */
    public function actionOrder($token)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        try {
            $user   = User::findOne(['token' => $token]);
            $order  = [];
            $models = Orders::findAll(['user_id' => $user->id]);

            foreach ($models as $model) {

                foreach ($model->orderedProducts as $orderedProduct) {
                    $product    = $orderedProduct->product;
                    $order[$orderedProduct->product_id] = [
                        'id'            => $product->id,
                        'name'          => $product->name,
                        'content'       => $product->content,
                        'image'         => \Yii::$app->request->hostInfo . $product->getImage(),
                        'price'         => $orderedProduct->detail->price,
                        'type'          => $orderedProduct->detail->type->name,
                        'attribute'     => $orderedProduct->detail->attribute0->name,
                        'order_id'      => $orderedProduct->order->id,
                        'order_date'    => $orderedProduct->order->created_at,
                        'order_sum'     => $orderedProduct->detail->price,
                    ];
                }
            }

            return array_values($order);
        }catch (\Exception $e){
            throw new ForbiddenHttpException('У вас недостаточно прав на просмотр');
        }
    }

    /**
     * view one order.
     * @param  $token
     * @param  int $id
     * @return mixed
     * @throws
     */
    public function actionView($token, $id)
    {
        $model  = Orders::getUserItemId($id);

        return $model;
    }
}