<?php

namespace api\controllers;

use api\models\FeedbackForm;
use yii\db\Exception;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpHeaderAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\HttpException;

class FeedbackController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => \yii\filters\Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                HttpHeaderAuth::class,
                QueryParamAuth::class
            ]
        ];


        return $behaviors;
    }

    public function actionCreate()
    {
        $model = new FeedbackForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $feedback = $model->create()) {

                $response   = [
                    'message'   => \Yii::t('main-message',
                        'Благодарим Вас за обратную связь! С Вами свяжутся наши менеджера!'),
                ];
                return $response;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}