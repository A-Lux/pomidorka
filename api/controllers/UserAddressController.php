<?php

namespace api\controllers;

use api\models\UserAddressForm;
use common\models\UserAddress;
use yii\db\Exception;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\HttpException;

class UserAddressController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],

        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model  = UserAddress::userAddress();

        return $model;
    }

    public function actionCreate()
    {
        $model = new UserAddressForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->createAddress()) {
                $data   = $model->dataUser($user);

                $response   = [
                    'message'       => \Yii::t('main-message', 'Вы успешно создали личный адрес!'),
                    'address'       => $data,
                ];
                return $response;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    public function actionUpdate()
    {
        $model = new UserAddressForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->updateAddress()) {
                $data   = $model->dataUser($user);

                $response   = [
                    'message'       => \Yii::t('main-message', 'Вы успешно обновили личные адреса!'),
                    'address'       => $data,
                ];
                return $response;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}