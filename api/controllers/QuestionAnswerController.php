<?php

namespace api\controllers;

use common\models\Faq;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class QuestionAnswerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model  = Faq::findAll(['status' => 1]);

        return $model;
    }
}
