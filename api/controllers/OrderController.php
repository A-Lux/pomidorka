<?php

namespace api\controllers;

use api\models\OrderForm;
use common\models\Orders;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OrderController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];


        return $behaviors;
    }

    /**
     * view all orders.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        $model  = Orders::getUserItems();

        return $model;
    }

    /**
     * view one order.
     * @param  int $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model  = Orders::getUserItemId($id);

        return $model;
    }

    public function actionCreate()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }
        
        $model  = new OrderForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(), '')){

            try {
                if ($order = $model->create()) {
                    if($order->typePay == 4){
                        $response   = [
                            'status'    => 1,
                            'message'   => 'Благодарим Вас за покупку!',
                            'url'       => '/api/pay?id=' . $order->id,
                        ];
                    }else{
                        $response   = [
                            'status' => 1,
                            'message' => 'Благодарим Вас за покупку!'];
                    }

                    return $response;
                } else {
                    $errors = $model->firstErrors;
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                throw new HttpException(403, $e->getMessage());
            }

        }
    }
}