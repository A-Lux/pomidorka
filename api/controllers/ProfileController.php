<?php

namespace api\controllers;

use api\models\ProfileForm;
use common\models\Client;
use common\models\User;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpHeaderAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\rest\OptionsAction;
use yii\web\HttpException;
use yii\web\ServerErrorHttpException;

class ProfileController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],

        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model  = User::userClient();

        return $model;
    }

    public function actionUpdate()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        $model = new ProfileForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->updateUser()) {
                $data   = $model->dataUser($user);

                $response   = [
                    'message'   => \Yii::t('main-message', 'Вы успешно обновили личные данные!'),
                    'user'      => $data,
                ];
                return $response;

            } else {
                $errors = $model->firstErrors;

                \Yii::$app->response->setStatusCode(422);
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    /**
     * @return User
     */
    private function findModel()
    {
        return User::findOne(\Yii::$app->user->id);
    }

}