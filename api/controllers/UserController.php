<?php

namespace api\controllers;

use api\components\ApiAuth;
use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\VerbFilter;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Response;

class UserController extends ActiveController
{
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => ApiAuth::className()
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'me' => ['get'],
                    'all' => ['get'],
                    'client' => ['get'],
                    'create' => ['post'],
                    'update' => ['put']
                ]
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['me','all','client','create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ]
        ];
    }

    public function actionMe()
    {
        $user = User::findIdentity(Yii::$app->user->id);

        if (!$user) {
            return [
                'success' => 0,
                'payload' => 'Some error occurred'
            ];
        }

        return [
            'success' => 1,
            'payload' => $user
        ];
    }

//
//    public function actions()
//    {
//        $actions = parent::actions();
//
//        unset($actions['create']);
//        unset($actions['update']);
//
//        return $actions;
//    }
}