<?php

namespace api\controllers;

use common\models\Catalog;
use common\models\Product;
use common\models\ProductDetails;
use common\models\ProductSizes;
use yii\filters\auth\CompositeAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\Response;

class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model  = Product::allProduct();
        $details    = ProductDetails::typeAttribute();

        return $details;
    }

    public function actionView($id)
    {
        $model  = $this->getProduct($id);

        return $model;
    }

    protected function getProduct($id)
    {
        $model      = Product::findOne(['id' => $id]);
        $details    = $model->detailsGroup($id);

        // $details    = $model->detailTest($id);

        $product    = [
            'id'        => $model->id,
            'name'      => $model->name,
            'content'   => $model->content,
            'image'     => $model->image
                ? \Yii::$app->request->hostInfo . $model->getImage()
                : \Yii::$app->request->hostInfo . '/backend/web/no-image.png',
            'details'   => $details,
        ];

        return $product;
    }

    public function actionTest($id)
    {
        $model      = Product::findOne(['id' => $id]);

        $details    = $model->detailTest($id);

        $product    = [
            'id'        => $model->id,
            'name'      => $model->name,
            'content'   => $model->content,
            'image'     => $model->image
                ? \Yii::$app->request->hostInfo . $model->getImage()
                : \Yii::$app->request->hostInfo . '/backend/web/no-image.png',
            'details'   => $details,
        ];

        return $product;
    }
}
