<?php

namespace api\controllers;

use common\models\Menu;
use common\models\PrivacyPolicy;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class PrivacyPolicyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return PrivacyPolicy::getOne();
    }
}