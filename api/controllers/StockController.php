<?php

namespace api\controllers;

use common\models\Stock;
use yii\rest\Controller;

class StockController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $stocks   = $this->getStocks();

        return $stocks;
    }

    public function actionView($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model  = $this->getStock($id);

        return $model;
    }

    protected function getStocks()
    {
        $stocks = [];
        $model  = Stock::findAll(['status' => 1]);

        foreach ($model as $value){
            $stocks[]   = [
                'id'    => $value->id,
                'name'  => $value->name,
                'title' => $value->title,
                'image' => $value->image
                    ? \Yii::$app->request->hostInfo .$value->getImage()
                    : \Yii::$app->request->hostInfo. '/backend/web/no-image.png',
            ];
        }

        return $stocks;
    }

    protected function getStock($id)
    {
        $model  = Stock::findOne(['id' => $id]);
        $stock  = [
            'id'        => $model->id,
            'name'      => $model->name,
            'title'     => $model->title,
            'content'   => $model->content,
            'image'     => $model->image
                ? \Yii::$app->request->hostInfo .$model->getImage()
                : \Yii::$app->request->hostInfo. '/backend/web/no-image.png',
        ];

        return $stock;
    }
}