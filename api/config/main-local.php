<?php

return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'yeIPpSGK3d_AvixgUSZFQk16U1cU_oTP',
        ],
    ],
];
