<?php
namespace api\models;

use common\models\Client;
use common\models\UserAddress;
use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{

    public $username;
    public $phone;
    public $email;
    public $password;
    public $password_repeat;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'              => 'Имя',
            'phone'                 => 'Номер телефона',
            'email'                 => 'Электронная почта',
            'password'              => 'Пароль',
            'password_repeat'       => 'Подтверждение пароля',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => Yii::t('main', 'Этот адрес электронной почты уже занят.')],

            ['username', 'string', 'max' => 255],
            ['username', 'required'],
            ['phone', 'required'],
            ['phone', 'validatePhone'],

            [['password', 'password_repeat'], 'required'],
            ['password', 'string', 'min' => 8],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => \Yii::t('main-error', 'Пароли не совпадают')],
        ];
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function signup()
    {
        if ($this->validate()) {
            $user           = new User();
            $user->email    = $this->email;
            $user->phone    = preg_replace("/[^0-9]/", "", $this->phone);
            $user->role     = BaseUser::ROLE_USER;
            $user->status   = BaseUser::STATUS_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();

            if($user->save()){
                $client             = new Client();
                $client->user_id    = $user->id;
                $client->username   = $this->username;
                $client->save();

                return $user;
            }

        }

        return false;
    }

    /**
     * Validates the phone.
     * This method serves as the inline validation for phone.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePhone($attribute, $params)
    {
        $phone          = preg_replace("/[^0-9]/", "", $this->phone);
        $users          = User::findAll(['phone' => $phone]);

        if ($users) {
            $this->addError($attribute, 'Этот номер телефона почты уже занят.');
        }
    }

    public function dataUser($model)
    {
        $userAddress    = UserAddress::find()->where(['user_id' => $model->id])->orderBy(['id' => SORT_ASC])->one();

        $user   = [
            'id'            => $model->id,
            'username'      => $model->client->username,
            'token'         => $model->token,
            'phone'         => $model->phone,
            'email'         => $model->email,
            'birthday'      => $model->client->birthday,
            'address'       => $userAddress ? $userAddress->address : '',
            'apartment'     => $userAddress ? $userAddress->apartment : '',
            'storey'        => $userAddress ? $userAddress->storey : '',
            'porch'         => $userAddress ? $userAddress->porch : '',
            'intercom'      => $userAddress ? $userAddress->intercom : '',
        ];

        return $user;
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'POMIDORKA robot'])
            ->setTo($this->email)
            ->setSubject('Вы зарегистрировались на сайте POMIDORKA')
            ->send();
    }
}
