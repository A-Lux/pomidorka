<?php

namespace api\models;

use common\models\Client;
use common\models\User;
use common\models\UserAddress;
use yii\base\Model;
use yii\web\HttpException;

class UserForm extends Model
{
    public $username;
    public $phone;
    public $email;
    public $birthday;
    public $address;
    public $apartment;
    public $storey;
    public $porch;
    public $intercom;

    private $_user;
    public $token;

    public $password;
    public $new_password;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username'              => 'Имя',
            'phone'                 => 'Номер телефона',
            'token'                 => 'Токен',
            'email'                 => 'Электронная почта',
            'birthday'              => 'День рождения',
            'address'               => 'Адрес доставки',
            'password'              => 'Старый пароль',
            'new_password'          => 'Новый пароль',
            'password_repeat'       => 'Подтверждение пароля',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['token'], 'required'],
            ['token', 'validateToken'],

            ['username', 'string', 'max' => 255],

            [['birthday'], 'string'],

            [['address'], 'string', 'max' => 255],

            [['apartment', 'storey', 'porch', 'intercom'], 'integer'],


            ['password', 'string', 'min' => 8],
            ['password', 'validatePassword'],

            ['new_password', 'string', 'min' => 8],

            ['new_password', 'required', 'when' => function($model) {
                return !empty($model->password);
            }],

        ];
    }

    /**
     * Update client
     * @return mixed
     * @throws
     */
    public function updateUser()
    {
        $user           = User::findOne(['token' => $this->token]);
        $client         = Client::findOne(['user_id' => $user->id]);
        $userAddress    = UserAddress::findOne(['user_id' => $user->id]);

        if(!$client){
            $client             = new Client();
            $client->user_id    = $user->id;
        }

        if(!$userAddress){
            $userAddress            = new UserAddress();
            $userAddress->user_id   = $user->id;
        }

        if ($this->validate()) {

            $client->username       = $this->username;
            $client->birthday       = $this->birthday;

            $userAddress->address       = $this->address;
            $userAddress->apartment     = $this->apartment;
            $userAddress->storey        = $this->storey;
            $userAddress->porch         = $this->porch;
            $userAddress->intercom      = $this->intercom;
            $userAddress->status        = 1;


            if(!empty($this->password) && !empty($this->new_password)){
                $user->setPassword($this->new_password);
            }

            if($client->save() && $user->save() && $userAddress->save()){

                return $user;
            }
        }

        return false;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findOne(['token' => $this->token]);

            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Старый пароль не соответсвует.');
            }
        }
    }

    /**
     * Validates the token.
     * This method serves as the inline validation for token.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateToken($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::findOne(['token' => $this->token]);

            if (!$user) {
                $this->addError($attribute, 'Токен параметр неправильный');
            }
        }
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => $this->token]);
        }

        return $this->_user;
    }

    public function dataUser($model)
    {
        $userAddress    = UserAddress::find()->where(['user_id' => $model->id])->orderBy(['id' => SORT_ASC])->one();

        $user   = [
            'id'            => $model->id,
            'username'      => $model->client->username,
            'token'         => $model->token,
            'phone'         => $model->phone,
            'email'         => $model->email,
            'birthday'      => $model->client->birthday,
            'address'       => $userAddress ? $userAddress->address : '',
            'apartment'     => $userAddress ? $userAddress->apartment : '',
            'storey'        => $userAddress ? $userAddress->storey : '',
            'porch'         => $userAddress ? $userAddress->porch : '',
            'intercom'      => $userAddress ? $userAddress->intercom : '',
        ];

        return $user;
    }
}