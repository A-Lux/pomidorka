<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_address}}`.
 */
class m200617_062329_create_user_address_table extends Migration
{
    public $table                   = 'user_address';
    public $userTable               = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'user_id'           => $this->integer()->defaultValue(0)->null(),
            'status'            => $this->integer()->defaultValue(0)->null(),
            'address'           => $this->string(255)->null(),
            'apartment'         => $this->string(255)->null(),
            'storey'            => $this->string(255)->null(),
            'porch'             => $this->string(255)->null(),
            'intercom'          => $this->string(255)->null(),
            'info'              => $this->text()->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->userTable}",
            "{{{$this->table}}}", 'user_id',
            "{{{$this->userTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            "fk_{$this->table}_{$this->userTable}",
            "{{{$this->userTable}}}");

        $this->dropTable("{{{$this->table}}}");
    }
}
