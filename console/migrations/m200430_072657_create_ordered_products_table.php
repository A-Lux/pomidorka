<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ordered_products}}`.
 */
class m200430_072657_create_ordered_products_table extends Migration
{
    public $table               = 'ordered_products';
    public $orderTable          = 'orders';
    public $productTable        = 'product';
    public $detailsTable        = 'product_details';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'order_id'                  => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'detail_id'                 => $this->integer()->null(),
            'count'                     => $this->integer()->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->orderTable}", "{{{$this->table}}}", 'order_id', "{{{$this->orderTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->detailsTable}", "{{{$this->table}}}", 'detail_id', "{{{$this->detailsTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->detailsTable}_{$this->table}", "{{{$this->detailsTable}}}");
        $this->dropForeignKey("fk_{$this->orderTable}_{$this->table}", "{{{$this->orderTable}}}");
        $this->dropForeignKey("fk_{$this->productTable}_{$this->table}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
