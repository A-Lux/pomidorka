<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%types}}`.
 */
class m200511_105458_create_types_table extends Migration
{
    public $table               = 'types';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'create_at'     => $this->timestamp()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
