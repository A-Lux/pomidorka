<?php

use yii\db\Migration;

/**
 * Class m200430_041111_add_columns_user_table
 */
class m200430_041111_add_columns_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'phone', $this->string()->null()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'phone');
    }
}
