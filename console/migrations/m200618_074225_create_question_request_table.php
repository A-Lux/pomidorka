<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%question_request}}`.
 */
class m200618_074225_create_question_request_table extends Migration
{
    public $table               = 'question_request';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'username'          => $this->string(255)->null(),
            'phone'             => $this->string(255)->null(),
            'email'             => $this->string(255)->null(),
            'theme'             => $this->string(255)->null(),
            'message'           => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
