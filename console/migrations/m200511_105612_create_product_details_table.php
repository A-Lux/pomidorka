<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_details}}`.
 */
class m200511_105612_create_product_details_table extends Migration
{
    public $table               = 'product_details';
    public $productTable        = 'product';
    public $typeTable           = 'types';
    public $attributeTable      = 'attributes';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'product_id'                => $this->integer()->null(),
            'type_id'                   => $this->integer()->null(),
            'attribute_id'              => $this->integer()->null(),
            'price'                     => $this->integer()->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->typeTable}", "{{{$this->table}}}", 'type_id', "{{{$this->typeTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->attributeTable}", "{{{$this->table}}}", 'attribute_id', "{{{$this->attributeTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->attributeTable}", "{{{$this->attributeTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->typeTable}", "{{{$this->typeTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
