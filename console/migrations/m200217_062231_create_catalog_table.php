<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog}}`.
 */
class m200217_062231_create_catalog_table extends Migration
{
    public $table               = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'parent_id'     => $this->integer()->null(),
            'status'        => $this->integer()->defaultValue(1)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'slug'          => $this->string(255)->notNull(),
            'content'       => $this->text()->null(),
            'image'         => $this->string(255)->null(),
            'level'         => $this->integer()->null(),
            'sort'          => $this->integer()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'create_at'     => $this->dateTime()->null(),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
