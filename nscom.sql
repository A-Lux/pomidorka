-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 24 2020 г., 07:04
-- Версия сервера: 5.7.20
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `nscom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about`
--

INSERT INTO `about` (`id`, `status`, `title`, `content`, `image`) VALUES
(1, 1, 'О компании', '<p>ТОО &ldquo;National Security &amp; Communication&rdquo; &ndash; торговая компания с широким профилем поставляемой продукции в сфере IT и технологий безопасности, представляющая на рынке Средней Азии наиболее востребованные и признанные мировым экспертным сообществом бренды.</p>\r\n\r\n<p>Мы успешно осуществляем свою деятельность на рынках Республики Казахстан и Кыргызстана, являясь проверенными и надежными поставщиками ряда ведущих национальных корпораций, государственных органов и крупнейших коммерческих организаций.</p>\r\n', '1581417131_company.png'),
(2, 2, 'Наши партнеры', '<p>Привлекая к реализации проектов известных международных партнеров, мы придерживаемся современных тенденции развития рынка IT, предполагая коренной пересмотр целей и задач развития бизнеса, что позволяет предлагать на рынке максимально актуальные решения.</p>\r\n\r\n<p>&ldquo;National Security&amp;Communication&rdquo; является бизнес-партнером ведущих мировых производителей:</p>\r\n', '1581417164_return-box.png'),
(3, 3, 'Наш подход', '<p>&laquo;Ноу-хау&raquo; Нашей компании &ndash; это предложение совершенно нового качества подхода к реализации проектов с нашими Партнерами, а также предложения решений, признанных во всем мире и отвечающих современным требования и вызовам. В течение долгого времени успешной работы, мы пользуемся безупречной репутацией у наших Партнеров, в том числе, благодаря безусловному выполнению взятых на себя обязательств по ассортименту и срокам поставки. Сегодня Клиентами и Партнерами компании являются 427 компаний, как в Республике Казахстан, так и за её пределами.</p>\r\n', '1581417214_about.png');

-- --------------------------------------------------------

--
-- Структура таблицы `about_item`
--

CREATE TABLE `about_item` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_item`
--

INSERT INTO `about_item` (`id`, `status`, `name`, `image`) VALUES
(1, 1, 'Информационная безопасность', '1581416548_категория-1.png'),
(2, 1, 'Сервера и системы хранения данных', '1581416569_категория-2.png'),
(3, 1, 'Системы видеонаблюдения', '1581416586_категория-3.png'),
(4, 1, 'Комьютерная и оргтехника', '1581416624_категория-4.png'),
(5, 1, 'Видео-конференционная связь', '1581416652_категория-5.png'),
(6, 1, 'Беспроводное питание', '1581416665_категория-6.png'),
(7, 1, 'Сетевое оборудование', '1581416679_категория-7.png'),
(8, 1, 'Взрыво-защищенное оборудование', '1581416691_категория-8.png'),
(9, 2, 'Широкий ассортимент оборудования', '1581416726_партнер1.png'),
(10, 2, 'Индивидуальный подход и гибкие финансовые условия', '1581416745_партнер2.png'),
(11, 2, 'Оптимальные цены на всю продукцию', '1581416776_партнер3.png'),
(12, 2, 'Семинары для Ваших заказчиков', '1581416799_партнер4.png'),
(13, 2, 'Информационную поддержку', '1581416835_патнер5.png'),
(14, 2, 'Техническую консультацию сертифицированных специалистов', '1581416852_партнер6.png');

-- --------------------------------------------------------

--
-- Структура таблицы `about_item_translation`
--

CREATE TABLE `about_item_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_item_translation`
--

INSERT INTO `about_item_translation` (`id`, `language`, `name`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', 'Информационная безопасность'),
(2, 'en', ''),
(2, 'kk', ''),
(2, 'ru', 'Сервера и системы хранения данных'),
(3, 'en', ''),
(3, 'kk', ''),
(3, 'ru', 'Системы видеонаблюдения'),
(4, 'en', ''),
(4, 'kk', ''),
(4, 'ru', 'Комьютерная и оргтехника'),
(5, 'en', ''),
(5, 'kk', ''),
(5, 'ru', 'Видео-конференционная связь'),
(6, 'en', ''),
(6, 'kk', ''),
(6, 'ru', 'Беспроводное питание'),
(7, 'en', ''),
(7, 'kk', ''),
(7, 'ru', 'Сетевое оборудование'),
(8, 'en', ''),
(8, 'kk', ''),
(8, 'ru', 'Взрыво-защищенное оборудование'),
(9, 'en', ''),
(9, 'kk', ''),
(9, 'ru', 'Широкий ассортимент оборудования'),
(10, 'en', ''),
(10, 'kk', ''),
(10, 'ru', 'Индивидуальный подход и гибкие финансовые условия'),
(11, 'en', ''),
(11, 'kk', ''),
(11, 'ru', 'Оптимальные цены на всю продукцию'),
(12, 'en', ''),
(12, 'kk', ''),
(12, 'ru', 'Семинары для Ваших заказчиков'),
(13, 'en', ''),
(13, 'kk', ''),
(13, 'ru', 'Информационную поддержку'),
(14, 'en', ''),
(14, 'kk', ''),
(14, 'ru', 'Техническую консультацию сертифицированных специалистов');

-- --------------------------------------------------------

--
-- Структура таблицы `about_partners`
--

CREATE TABLE `about_partners` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_partners`
--

INSERT INTO `about_partners` (`id`, `status`, `name`, `link`) VALUES
(1, 1, 'Panasonic', ''),
(2, 1, 'Вэлан', ''),
(3, 1, 'Tripp-Lite', ''),
(4, 1, 'Hewlett-Packard', ''),
(5, 1, 'Lenovo', ''),
(6, 1, 'Samsung', ''),
(7, 1, 'Acer', ''),
(8, 1, 'ASUS', ''),
(9, 1, 'DELL', '');

-- --------------------------------------------------------

--
-- Структура таблицы `about_translation`
--

CREATE TABLE `about_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `about_translation`
--

INSERT INTO `about_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'О компании', '<p>ТОО &ldquo;National Security &amp; Communication&rdquo; &ndash; торговая компания с широким профилем поставляемой продукции в сфере IT и технологий безопасности, представляющая на рынке Средней Азии наиболее востребованные и признанные мировым экспертным сообществом бренды.</p>\r\n\r\n<p>Мы успешно осуществляем свою деятельность на рынках Республики Казахстан и Кыргызстана, являясь проверенными и надежными поставщиками ряда ведущих национальных корпораций, государственных органов и крупнейших коммерческих организаций.</p>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Наши партнеры', '<p>Привлекая к реализации проектов известных международных партнеров, мы придерживаемся современных тенденции развития рынка IT, предполагая коренной пересмотр целей и задач развития бизнеса, что позволяет предлагать на рынке максимально актуальные решения.</p>\r\n\r\n<p>&ldquo;National Security&amp;Communication&rdquo; является бизнес-партнером ведущих мировых производителей:</p>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'Наш подход', '<p>&laquo;Ноу-хау&raquo; Нашей компании &ndash; это предложение совершенно нового качества подхода к реализации проектов с нашими Партнерами, а также предложения решений, признанных во всем мире и отвечающих современным требования и вызовам. В течение долгого времени успешной работы, мы пользуемся безупречной репутацией у наших Партнеров, в том числе, благодаря безусловному выполнению взятых на себя обязательств по ассортименту и срокам поставки. Сегодня Клиентами и Партнерами компании являются 427 компаний, как в Республике Казахстан, так и за её пределами.</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner`
--

INSERT INTO `banner` (`id`, `title`, `image`, `sort`) VALUES
(1, 'Spirio', '1580209907_фон-слайдера.png', 1),
(5, 'test', '1580817198_service__img.png', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `banner_translation`
--

CREATE TABLE `banner_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `banner_translation`
--

INSERT INTO `banner_translation` (`id`, `language`, `title`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', 'Spirio'),
(5, 'en', ''),
(5, 'kk', ''),
(5, 'ru', 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statusButton` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `brand`
--

INSERT INTO `brand` (`id`, `name`, `title`, `content`, `image`, `file`, `statusButton`, `slug`) VALUES
(1, 'Western Digital', 'Western Digital', '<p>ТОО &ldquo;Western Digital&rdquo; &ndash; торговая компания с широким профилем поставляемой продукции в сфере IT и технологий безопасности, представляющая на рынке Средней Азии наиболее востребованные и признанные мировым экспертным сообществом бренды.</p>\r\n\r\n<p>Мы успешно осуществляем свою деятельность на рынках Республики Казахстан и Кыргызстана, являясь проверенными и надежными поставщиками ряда ведущих национальных корпораций, государственных органов и крупнейших коммерческих организаций.</p>\r\n', '1581506931_western-digital.png', '1581506931_Новый текстовый документ.txt', 1, 'western-digital');

-- --------------------------------------------------------

--
-- Структура таблицы `brand_translation`
--

CREATE TABLE `brand_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `brand_translation`
--

INSERT INTO `brand_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Western Digital', '<p>ТОО &ldquo;Western Digital&rdquo; &ndash; торговая компания с широким профилем поставляемой продукции в сфере IT и технологий безопасности, представляющая на рынке Средней Азии наиболее востребованные и признанные мировым экспертным сообществом бренды.</p>\r\n\r\n<p>Мы успешно осуществляем свою деятельность на рынках Республики Казахстан и Кыргызстана, являясь проверенными и надежными поставщиками ряда ведущих национальных корпораций, государственных органов и крупнейших коммерческих организаций.</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci,
  `create_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `parent_id`, `status`, `name`, `slug`, `content`, `image`, `sort`, `metaName`, `metaDesc`, `metaKey`, `create_at`) VALUES
(1, NULL, 1, 'Сетевое оборудование', 'setevoe-oborudovanie', '<p>Сетевое оборудование</p>\r\n', '1582012340_товар1.svg', 1, '', '', '', '2020-02-18 13:58:00'),
(2, NULL, 1, 'Профессиональные дисплеи', 'professionalynie-displei', '<p>Профессиональные дисплеи</p>\r\n', '1582012380_товар-2.svg', 2, '', '', '', '2020-02-18 13:59:00'),
(3, NULL, 1, 'Системы видеонаблюдения', 'sistemi-videonablyudeniya', '<p>Системы видеонаблюдения</p>\r\n', '1582012414_товар3.svg', 3, '', '', '', '2020-02-18 13:59:00'),
(4, NULL, 1, 'Защищенные ноутбуки', 'zashtishtennie-noutbuki', '<p>Защищенные ноутбуки</p>\r\n', '1582012465_товар4.svg', 4, '', '', '', '2020-02-18 13:59:00'),
(5, NULL, 1, 'Системы бесперебойного питания', 'sistemi-bespereboynogo-pitaniya', '<p>Системы бесперебойного питания</p>\r\n', '1582012495_товар-5.svg', 5, '', '', '', '2020-02-18 13:59:00'),
(6, 1, 1, 'Адаптеры', 'adapteri', '<p>адаптеры</p>\r\n', '', 6, '', '', '', '2020-02-18 14:04:00'),
(7, 1, 1, 'Беспроводное оборудование', 'besprovodnoe-oborudovanie', '', '', 7, '', '', '', '2020-02-18 14:05:00'),
(8, 1, 1, 'Коммутаторы', 'kommutatori', '', '', 8, '', '', '', '2020-02-18 14:06:00'),
(9, 5, 1, 'ИБП', 'ibp', '', '', 9, '', '', '', '2020-02-18 14:07:00'),
(10, 5, 1, 'PDU', 'pdu', '', '', 10, '', '', '', '2020-02-18 14:07:00'),
(11, 5, 1, 'Сетевые фильтры', 'setevie-filytri', '', '', 11, '', '', '', '2020-02-18 14:07:00'),
(12, 5, 1, 'Стабилизаторы', 'stabilizatori', '', '', 12, '', '', '', '2020-02-18 14:08:00'),
(13, 8, 1, 'Промышленные коммутаторы', 'promishlennie-kommutatori', '', '', 13, '', '', '', '2020-02-18 14:08:00'),
(14, 9, 1, 'Линейно-итеративные', 'lineyno-iterativnie', '', '', 14, '', '', '', '2020-02-18 14:09:00'),
(15, 9, 1, 'OnLine', 'online', '', '', 15, '', '', '', '2020-02-18 14:57:00'),
(16, 10, 1, 'Однофазные', 'odnofaznie', '', '', 16, '', '', '', '2020-02-18 14:58:00'),
(17, 10, 1, 'Трехфазные', 'trehfaznie', '', '', 17, '', '', '', '2020-02-18 14:58:00'),
(18, 9, 1, 'Батареи для ИБП', 'batarei-dlya-ibp', '', '', 18, '', '', '', '2020-02-18 14:58:00'),
(19, 9, 1, 'Сменные батареи', 'smennie-batarei', '', '', 19, '', '', '', '2020-02-18 14:59:00');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_translation`
--

CREATE TABLE `catalog_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `catalog_translation`
--

INSERT INTO `catalog_translation` (`id`, `language`, `name`, `content`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'en', '', '', '', '', ''),
(1, 'kk', '', '', '', '', ''),
(1, 'ru', 'Сетевое оборудование', '<p>Сетевое оборудование</p>\r\n', '', '', ''),
(2, 'en', '', '', '', '', ''),
(2, 'kk', '', '', '', '', ''),
(2, 'ru', 'Профессиональные дисплеи', '<p>Профессиональные дисплеи</p>\r\n', '', '', ''),
(3, 'en', '', '', '', '', ''),
(3, 'kk', '', '', '', '', ''),
(3, 'ru', 'Системы видеонаблюдения', '<p>Системы видеонаблюдения</p>\r\n', '', '', ''),
(4, 'en', '', '', '', '', ''),
(4, 'kk', '', '', '', '', ''),
(4, 'ru', 'Защищенные ноутбуки', '<p>Защищенные ноутбуки</p>\r\n', '', '', ''),
(5, 'en', '', '', '', '', ''),
(5, 'kk', '', '', '', '', ''),
(5, 'ru', 'Системы бесперебойного питания', '<p>Системы бесперебойного питания</p>\r\n', '', '', ''),
(6, 'en', '', '', '', '', ''),
(6, 'kk', '', '', '', '', ''),
(6, 'ru', 'Адаптеры', '<p>адаптеры</p>\r\n', '', '', ''),
(7, 'en', '', '', '', '', ''),
(7, 'kk', '', '', '', '', ''),
(7, 'ru', 'Беспроводное оборудование', '', '', '', ''),
(8, 'en', '', '', '', '', ''),
(8, 'kk', '', '', '', '', ''),
(8, 'ru', 'Коммутаторы', '', '', '', ''),
(9, 'en', '', '', '', '', ''),
(9, 'kk', '', '', '', '', ''),
(9, 'ru', 'ИБП', '', '', '', ''),
(10, 'en', '', '', '', '', ''),
(10, 'kk', '', '', '', '', ''),
(10, 'ru', 'PDU', '', '', '', ''),
(11, 'en', '', '', '', '', ''),
(11, 'kk', '', '', '', '', ''),
(11, 'ru', 'Сетевые фильтры', '', '', '', ''),
(12, 'en', '', '', '', '', ''),
(12, 'kk', '', '', '', '', ''),
(12, 'ru', 'Стабилизаторы', '', '', '', ''),
(13, 'en', '', '', '', '', ''),
(13, 'kk', '', '', '', '', ''),
(13, 'ru', 'Промышленные коммутаторы', '', '', '', ''),
(14, 'en', '', '', '', '', ''),
(14, 'kk', '', '', '', '', ''),
(14, 'ru', 'Линейно-итеративные', '', '', '', ''),
(15, 'en', '', '', '', '', ''),
(15, 'kk', '', '', '', '', ''),
(15, 'ru', 'OnLine', '', '', '', ''),
(16, 'en', '', '', '', '', ''),
(16, 'kk', '', '', '', '', ''),
(16, 'ru', 'Однофазные', '', '', '', ''),
(17, 'en', '', '', '', '', ''),
(17, 'kk', '', '', '', '', ''),
(17, 'ru', 'Трехфазные', '', '', '', ''),
(18, 'en', '', '', '', '', ''),
(18, 'kk', '', '', '', '', ''),
(18, 'ru', 'Батареи для ИБП', '', '', '', ''),
(19, 'en', '', '', '', '', ''),
(19, 'kk', '', '', '', '', ''),
(19, 'ru', 'Сменные батареи', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `city_translation`
--

CREATE TABLE `city_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `work_phone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_phone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `client`
--

INSERT INTO `client` (`id`, `user_id`, `status`, `username`, `company`, `region`, `city`, `work_phone`, `mobile_phone`) VALUES
(1, 11, 1, 'Amir', 'Company', 'Spain', 'Madrid', '3123123123', '213123123');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `header_phone` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` text COLLATE utf8_unicode_ci,
  `iframe` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `address`, `header_phone`, `contact_phone`, `iframe`) VALUES
(1, '<h5>05000, Республика Казахстан, г. Алматы, пр. Райымбека 162</h5>\r\n                        <p>(На территории завода им. Крючкова)</p>', '+7 431 444 44 33', '<p>+7 [727] 279 76 92 <br>+7 [727] 279 93 83</p>', '<iframe src=\"https://yandex.ru/map-widget/v1/?um=constructor%3Aa6143d33c57358409764ababc8a914f3aa4ffffb0b9344add05023f9fc1b3dbb&amp;source=constructor\"\r\n                                width=\"100%\" height=\"342\" frameborder=\"0\"></iframe>');

-- --------------------------------------------------------

--
-- Структура таблицы `contact_translation`
--

CREATE TABLE `contact_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `contact_translation`
--

INSERT INTO `contact_translation` (`id`, `language`, `address`) VALUES
(1, 'en', ''),
(1, 'kk', '');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `delivery`
--

INSERT INTO `delivery` (`id`, `title`, `content`) VALUES
(1, 'Самовывоз', '<div class=\"export-inner\">\r\n<p>Забрать товар самостоятельно в офисе интернет-магазина вы сможете по следующим адресам:</p>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D0%B0%D0%B4%D1%80%D0%B5%D1%81.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>Г Алматы ул Райымбека 162</h3>\r\n\r\n<h3>Г Нур-Султан ул. Кабанбай Батыра 6/5</h3>\r\n</div>\r\n</div>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D1%87%D0%B0%D1%81%D1%8B.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>понедельник - пятница: с 9.00 до 18.00</h3>\r\n</div>\r\n</div>\r\n\r\n<p>Вместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для юридических лиц ‒ оригинал счёта, накладную.<br />\r\nПри получении товара, необходимо проверьте его внешний вид и комплектацию.</p>\r\n</div>\r\n'),
(2, 'Курьерская доставка', '<div class=\"courier-inner\">\r\n<p>Услуга курьерской доставки доступна в г. Алматы и в г. Нур-Султан.<br />\r\nНаш курьер привезет заказ по указанному адресу с в течение суток с момента оплаты заказа. Вы<br />\r\nтакже можете выбрать удобный для вас интервал времени в период:</p>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D1%87%D0%B0%D1%81%D1%8B.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>понедельник - пятница: с 9.00 до 18.00</h3>\r\n</div>\r\n</div>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/money.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h5>Доставка по указанным городам</h5>\r\n\r\n<div class=\"d-flex export-cost\">\r\n<h5>От 500 &ndash; 300 000 тенге составит <strong>2000 тг</strong></h5>\r\n</div>\r\n\r\n<div class=\"d-flex\">\r\n<h5>Свыше 300 000 тенге доставка <strong>бесплатно</strong></h5>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>Курьер дополнительно свяжется с вами перед выездом (приблизительно за 1 час).<br />\r\nВместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для<br />\r\nюридических лиц оригинал счёта, накладную.<br />\r\nПосле получении товара, проверьте его внешний вид и комплектацию.</p>\r\n</div>\r\n'),
(3, 'Через транспортную компанию', '<div class=\"transport-inner\">\r\n<p>Если вы территориально находитесь за пределами Алматы и Нур-Султан, то воспользуйтесь услугами доставки через транспортные компании. Мы сотрудничаем со всеми транспортными компаниями в Казахстане.</p>\r\n\r\n<p>Отслеживать статус заказа вы сможете на сайте выбранной транспортной компании. Указать транспортную компанию можно в процессе оформления заказа. Вместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для юридических лиц ‒ оригинал счёта, накладную. После получении товара, проверьте его внешний вид и комплектацию</p>\r\n\r\n<p>Все претензии по поводу поврежденной упаковки или неисправности товара, возникшие в процессе транспортировки, предъявляются выбранной транспортной компании. Стоимость доставки в данном случае зависит от региона доставки и веса товара.</p>\r\n</div>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_translation`
--

CREATE TABLE `delivery_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `delivery_translation`
--

INSERT INTO `delivery_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Самовывоз', '<div class=\"export-inner\">\r\n<p>Забрать товар самостоятельно в офисе интернет-магазина вы сможете по следующим адресам:</p>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D0%B0%D0%B4%D1%80%D0%B5%D1%81.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>Г Алматы ул Райымбека 162</h3>\r\n\r\n<h3>Г Нур-Султан ул. Кабанбай Батыра 6/5</h3>\r\n</div>\r\n</div>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D1%87%D0%B0%D1%81%D1%8B.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>понедельник - пятница: с 9.00 до 18.00</h3>\r\n</div>\r\n</div>\r\n\r\n<p>Вместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для юридических лиц ‒ оригинал счёта, накладную.<br />\r\nПри получении товара, необходимо проверьте его внешний вид и комплектацию.</p>\r\n</div>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Курьерская доставка', '<div class=\"courier-inner\">\r\n<p>Услуга курьерской доставки доступна в г. Алматы и в г. Нур-Султан.<br />\r\nНаш курьер привезет заказ по указанному адресу с в течение суток с момента оплаты заказа. Вы<br />\r\nтакже можете выбрать удобный для вас интервал времени в период:</p>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/%D1%87%D0%B0%D1%81%D1%8B.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h3>понедельник - пятница: с 9.00 до 18.00</h3>\r\n</div>\r\n</div>\r\n\r\n<div class=\"export-links d-flex\">\r\n<div class=\"col-xl-1 col-1\"><img alt=\"\" src=\"/images/icons/money.png\" /></div>\r\n\r\n<div class=\"col-xl-11 col-11\">\r\n<h5>Доставка по указанным городам</h5>\r\n\r\n<div class=\"d-flex export-cost\">\r\n<h5>От 500 &ndash; 300 000 тенге составит <strong>2000 тг</strong></h5>\r\n</div>\r\n\r\n<div class=\"d-flex\">\r\n<h5>Свыше 300 000 тенге доставка <strong>бесплатно</strong></h5>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<p>Курьер дополнительно свяжется с вами перед выездом (приблизительно за 1 час).<br />\r\nВместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для<br />\r\nюридических лиц оригинал счёта, накладную.<br />\r\nПосле получении товара, проверьте его внешний вид и комплектацию.</p>\r\n</div>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'Через транспортную компанию', '<div class=\"transport-inner\">\r\n<p>Если вы территориально находитесь за пределами Алматы и Нур-Султан, то воспользуйтесь услугами доставки через транспортные компании. Мы сотрудничаем со всеми транспортными компаниями в Казахстане.</p>\r\n\r\n<p>Отслеживать статус заказа вы сможете на сайте выбранной транспортной компании. Указать транспортную компанию можно в процессе оформления заказа. Вместе с заказом вы получите комплект документов: для частных лиц ‒ товарный чек, для юридических лиц ‒ оригинал счёта, накладную. После получении товара, проверьте его внешний вид и комплектацию</p>\r\n\r\n<p>Все претензии по поводу поврежденной упаковки или неисправности товара, возникшие в процессе транспортировки, предъявляются выбранной транспортной компании. Стоимость доставки в данном случае зависит от региона доставки и веса товара.</p>\r\n</div>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `filter_attr`
--

CREATE TABLE `filter_attr` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `filter_attr`
--

INSERT INTO `filter_attr` (`id`, `name`, `type`, `position`, `sort`) VALUES
(1, 'Модель', 1, 1, 1),
(2, 'Процессор', 1, 1, 2),
(3, 'Операционная система', 1, 1, 3),
(4, 'Производитель', 1, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_attr_translation`
--

CREATE TABLE `filter_attr_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `filter_attr_translation`
--

INSERT INTO `filter_attr_translation` (`id`, `language`, `name`) VALUES
(1, 'ru', 'Модель'),
(2, 'ru', 'Процессор'),
(3, 'ru', 'Операционная система'),
(4, 'ru', 'Производитель');

-- --------------------------------------------------------

--
-- Структура таблицы `filter_entity`
--

CREATE TABLE `filter_entity` (
  `id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `filter_entity`
--

INSERT INTO `filter_entity` (`id`, `value_id`, `product_id`) VALUES
(1, 1, 1),
(2, 6, 1),
(3, 9, 1),
(4, 11, 1),
(5, 2, 2),
(6, 6, 2),
(7, 11, 2),
(8, 1, 4),
(9, 8, 4),
(10, 12, 3),
(11, 13, 5),
(12, 11, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_value`
--

CREATE TABLE `filter_value` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `filter_value`
--

INSERT INTO `filter_value` (`id`, `attribute_id`, `name`, `sort`) VALUES
(1, 1, 'AMD A Series', 1),
(2, 1, 'Intel Celeron', 2),
(3, 1, 'AMD Ryzen 5', 3),
(4, 1, 'Intel Core i7', 4),
(5, 2, 'Model AMD A Series ', 5),
(6, 2, 'Model Intel', 6),
(7, 2, 'Model Ryzen', 7),
(8, 3, 'система Series', 8),
(9, 3, 'система Celeron', 9),
(10, 4, 'Производитель AMD', 10),
(11, 4, 'Производитель  Intel', 11),
(12, 4, 'Производитель Core', 12),
(13, 4, 'Производитель Ryzen', 13);

-- --------------------------------------------------------

--
-- Структура таблицы `filter_value_translation`
--

CREATE TABLE `filter_value_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `filter_value_translation`
--

INSERT INTO `filter_value_translation` (`id`, `language`, `name`) VALUES
(1, 'en', ''),
(1, 'kk', ''),
(1, 'ru', 'AMD A Series'),
(2, 'en', ''),
(2, 'kk', ''),
(2, 'ru', 'Intel Celeron'),
(3, 'en', ''),
(3, 'kk', ''),
(3, 'ru', 'AMD Ryzen 5'),
(4, 'en', ''),
(4, 'kk', ''),
(4, 'ru', 'Intel Core i7'),
(5, 'en', ''),
(5, 'kk', ''),
(5, 'ru', 'Model AMD A Series '),
(6, 'en', ''),
(6, 'kk', ''),
(6, 'ru', 'Model Intel'),
(7, 'en', ''),
(7, 'kk', ''),
(7, 'ru', 'Model Ryzen'),
(8, 'en', ''),
(8, 'kk', ''),
(8, 'ru', 'система Series'),
(9, 'en', ''),
(9, 'kk', ''),
(9, 'ru', 'система Celeron'),
(10, 'en', ''),
(10, 'kk', ''),
(10, 'ru', 'Производитель AMD'),
(11, 'en', ''),
(11, 'kk', ''),
(11, 'ru', 'Производитель  Intel'),
(12, 'en', ''),
(12, 'kk', ''),
(12, 'ru', 'Производитель Core'),
(13, 'en', ''),
(13, 'kk', ''),
(13, 'ru', 'Производитель Ryzen');

-- --------------------------------------------------------

--
-- Структура таблицы `guarantee`
--

CREATE TABLE `guarantee` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `guarantee`
--

INSERT INTO `guarantee` (`id`, `title`, `content`) VALUES
(1, 'Как осуществить <br> возврат', '<div class=\"return-inner\">\r\n<div class=\"return-content d-flex\">\r\n<div class=\"col-xl-8\">\r\n<div class=\"return-txt\">\r\n<p>Товар, приобретенный в интернет-магазине NScom, является фирменным и имеет все сопутствующие документы, гарантирующие качество продукции. Товар доставляется с гарантийным талоном, который является документом, обеспечивающим бесплатный ремонт, а также замену запчастей, если в течение гарантийного периода возникнет такая необходимость. В случае возникновения проблем с товаром, которые могут включать возможный производственный брак, необходимо обратиться в сервисный центр. Гарантийный талон дает право на бесплатное сервисное обслуживание в течение указанного периода (в зависимости от условий производителя) с момента осуществления покупки.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 garant-img\">\r\n<div class=\"return-img\"><img alt=\"\" src=\"/images/image/return-box.png\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-12\">\r\n<div class=\"return-txt-list\">\r\n<div class=\"return-txt\">\r\n<p>В случае, когда сервисный центр не может решить возникшую ситуацию, а также в случае отсутствия сервисного центра в городе, необходимо обратиться к менеджерам интернет-магазина Ruba. При обращении через электронную почту, укажите в теме письма &laquo;Гарантийное обслуживание&raquo;.</p>\r\n\r\n<p>В случае, если возникшая ситуация не была исправлена дистанционно, товар необходимо отправить по почте. Почтовые услуги оплачиваются покупателем.</p>\r\n\r\n<h2>Каждый случай рассматривается индивидуально, однако, необходимым условием для выяснения неисправности товара и отправки его по почте является сумма ниже перечисленных действий:</h2>\r\n</div>\r\n\r\n<div class=\"return-nav\">\r\n<ul>\r\n	<li>Письменное обращение в интернет-магазин NScom</li>\r\n	<li>Письменно подтвержденное согласование отправки товара с менеджером интернет-магазина;</li>\r\n	<li>Приложение гарантийного талона с подписью покупателя к отправляемому товару;</li>\r\n	<li>Наличие неповрежденного гарантийного маркера на товаре;</li>\r\n	<li>Полная комплектация товара.</li>\r\n	<li>Соблюдение условий обмена/возврата товара, которые описаны в гарантийном талоне.</li>\r\n	<li>Наличие упаковки, подходящей для безопасной отправки данного типа товара.=</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(2, 'Гарантийный срок <br> обслуживания', '<div class=\"srok-inner\">\r\n<div class=\"srok-content\">\r\n<div class=\"col-xl-8 col-12\">\r\n<div class=\"return-txt srok-txt\">\r\n<p>Как и гарантийные условия, срок сервисного обслуживания указан в гарантийном талоне. Он начинает действовать с момента отправки товара покупателю, либо при получении товара заказчиком. На время проведения гарантийного ремонта, гарантийный срок продлевается.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 col-12 garant-img\">\r\n<div class=\"srok-img\"><img alt=\"\" src=\"/images/image/%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(3, 'Условия возврата и обмена дефектного товара', '<div class=\"condition-inner\">\r\n<div class=\"condition-content\">\r\n<div class=\"col-xl-8 col-12\">\r\n<div class=\"return-txt condition-txt\">\r\n<p>Если в приобретенном товаре был обнаружен брак либо выявлено его несоответствие заявленным характеристикам, его возврат или обмен возможен при условии предоставления Акта, подтверждающего наличие дефекта в приобретенном товаре. Данный Акт может быть предоставлен экспертной организацией, например, Торгово-промышленной палатой или любой другой аналогичной комиссией. В случае установления экспертами несоответствий с заявленными характеристиками, а также, если проблемы с товаром возникли до получения его покупателем, продавец осуществляет возврат денежных средств.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4  col-12 garant-img\">\r\n<div class=\"condition-img\"><img alt=\"\" src=\"/images/image/%D1%83%D1%81%D0%BB%D0%BE%D0%B2%D0%B8%D1%8F.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(4, 'Права покупателя: возврат и обмен', '<div class=\"exchange-inner\">\r\n<div class=\"exchange-content\">\r\n<div class=\"col-xl-8\">\r\n<div class=\"return-txt exchange-txt\">\r\n<p>Товар, приобретенный в интернет-магазине Ruba, не позднее 14 календарных дней может быть обменен. Этот срок начинает действовать с момента получения товара покупателем на почте либо от курьера. Обмен или возврат возможен только при условии предоставления сопутствующей документации (оригиналов), в которой зафиксирована дата получения товара, а также дата его оправки продавцом. Стоимость почтовых услуг по обмену или возврату товара берет на себя покупатель. Возврат оплаты покупки осуществляется почтовым переводом, комиссионный сбор, а также стоимость доставки в этом случае не возвращаются.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 garant-img \">\r\n<div class=\"exchange-img\"><img alt=\"\" src=\"/images/image/%D0%BF%D1%80%D0%B0%D0%B2%D0%B0-%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B8.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `guarantee_translation`
--

CREATE TABLE `guarantee_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `guarantee_translation`
--

INSERT INTO `guarantee_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Как осуществить <br> возврат', '<div class=\"return-inner\">\r\n<div class=\"return-content d-flex\">\r\n<div class=\"col-xl-8\">\r\n<div class=\"return-txt\">\r\n<p>Товар, приобретенный в интернет-магазине NScom, является фирменным и имеет все сопутствующие документы, гарантирующие качество продукции. Товар доставляется с гарантийным талоном, который является документом, обеспечивающим бесплатный ремонт, а также замену запчастей, если в течение гарантийного периода возникнет такая необходимость. В случае возникновения проблем с товаром, которые могут включать возможный производственный брак, необходимо обратиться в сервисный центр. Гарантийный талон дает право на бесплатное сервисное обслуживание в течение указанного периода (в зависимости от условий производителя) с момента осуществления покупки.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 garant-img\">\r\n<div class=\"return-img\"><img alt=\"\" src=\"/images/image/return-box.png\" /></div>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-12\">\r\n<div class=\"return-txt-list\">\r\n<div class=\"return-txt\">\r\n<p>В случае, когда сервисный центр не может решить возникшую ситуацию, а также в случае отсутствия сервисного центра в городе, необходимо обратиться к менеджерам интернет-магазина Ruba. При обращении через электронную почту, укажите в теме письма &laquo;Гарантийное обслуживание&raquo;.</p>\r\n\r\n<p>В случае, если возникшая ситуация не была исправлена дистанционно, товар необходимо отправить по почте. Почтовые услуги оплачиваются покупателем.</p>\r\n\r\n<h2>Каждый случай рассматривается индивидуально, однако, необходимым условием для выяснения неисправности товара и отправки его по почте является сумма ниже перечисленных действий:</h2>\r\n</div>\r\n\r\n<div class=\"return-nav\">\r\n<ul>\r\n	<li>Письменное обращение в интернет-магазин NScom</li>\r\n	<li>Письменно подтвержденное согласование отправки товара с менеджером интернет-магазина;</li>\r\n	<li>Приложение гарантийного талона с подписью покупателя к отправляемому товару;</li>\r\n	<li>Наличие неповрежденного гарантийного маркера на товаре;</li>\r\n	<li>Полная комплектация товара.</li>\r\n	<li>Соблюдение условий обмена/возврата товара, которые описаны в гарантийном талоне.</li>\r\n	<li>Наличие упаковки, подходящей для безопасной отправки данного типа товара.=</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Гарантийный срок <br> обслуживания', '<div class=\"srok-inner\">\r\n<div class=\"srok-content\">\r\n<div class=\"col-xl-8 col-12\">\r\n<div class=\"return-txt srok-txt\">\r\n<p>Как и гарантийные условия, срок сервисного обслуживания указан в гарантийном талоне. Он начинает действовать с момента отправки товара покупателю, либо при получении товара заказчиком. На время проведения гарантийного ремонта, гарантийный срок продлевается.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 col-12 garant-img\">\r\n<div class=\"srok-img\"><img alt=\"\" src=\"/images/image/%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'Условия возврата и обмена дефектного товара', '<div class=\"condition-inner\">\r\n<div class=\"condition-content\">\r\n<div class=\"col-xl-8 col-12\">\r\n<div class=\"return-txt condition-txt\">\r\n<p>Если в приобретенном товаре был обнаружен брак либо выявлено его несоответствие заявленным характеристикам, его возврат или обмен возможен при условии предоставления Акта, подтверждающего наличие дефекта в приобретенном товаре. Данный Акт может быть предоставлен экспертной организацией, например, Торгово-промышленной палатой или любой другой аналогичной комиссией. В случае установления экспертами несоответствий с заявленными характеристиками, а также, если проблемы с товаром возникли до получения его покупателем, продавец осуществляет возврат денежных средств.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4  col-12 garant-img\">\r\n<div class=\"condition-img\"><img alt=\"\" src=\"/images/image/%D1%83%D1%81%D0%BB%D0%BE%D0%B2%D0%B8%D1%8F.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n'),
(4, 'en', '', ''),
(4, 'kk', '', ''),
(4, 'ru', 'Права покупателя: возврат и обмен', '<div class=\"exchange-inner\">\r\n<div class=\"exchange-content\">\r\n<div class=\"col-xl-8\">\r\n<div class=\"return-txt exchange-txt\">\r\n<p>Товар, приобретенный в интернет-магазине Ruba, не позднее 14 календарных дней может быть обменен. Этот срок начинает действовать с момента получения товара покупателем на почте либо от курьера. Обмен или возврат возможен только при условии предоставления сопутствующей документации (оригиналов), в которой зафиксирована дата получения товара, а также дата его оправки продавцом. Стоимость почтовых услуг по обмену или возврату товара берет на себя покупатель. Возврат оплаты покупки осуществляется почтовым переводом, комиссионный сбор, а также стоимость доставки в этом случае не возвращаются.</p>\r\n</div>\r\n</div>\r\n\r\n<div class=\"col-xl-4 garant-img \">\r\n<div class=\"exchange-img\"><img alt=\"\" src=\"/images/image/%D0%BF%D1%80%D0%B0%D0%B2%D0%B0-%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%BA%D0%B8.png\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(128) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `language`
--

INSERT INTO `language` (`id`, `name`, `code`, `image`) VALUES
(1, 'казахский', 'kk', '1580468762_kz_flag.png'),
(2, 'русский', 'ru', '1580468811_ru_flag.png'),
(3, 'english', 'en', '1580468824_uk_flag.png');

-- --------------------------------------------------------

--
-- Структура таблицы `logo`
--

CREATE TABLE `logo` (
  `id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `logo`
--

INSERT INTO `logo` (`id`, `position`, `image`) VALUES
(1, 2, '1580211665_2087075092656.png');

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `status`, `position`, `sort`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'Главная', '/', 0, 1, 1, 'Главная', '', ''),
(2, 'О компании', '/about', 1, 1, 2, 'О компании', '', ''),
(3, 'Контакты', '/contact', 1, 1, 3, 'Контакты', '', ''),
(4, 'Новости', '/news', 1, 1, 4, 'Новости', '', ''),
(5, 'Оплата и доставка', '/delivery', 1, 1, 5, 'Оплата и доставка', '', ''),
(6, 'Жалобы и преложения', '/suggestion', 1, 1, 6, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_translation`
--

CREATE TABLE `menu_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `menu_translation`
--

INSERT INTO `menu_translation` (`id`, `language`, `name`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'en', '', '', '', ''),
(1, 'kk', '', '', '', ''),
(1, 'ru', 'Главная', '', '', ''),
(2, 'en', '', '', '', ''),
(2, 'kk', '', '', '', ''),
(2, 'ru', 'О компании', '', '', ''),
(3, 'en', '', '', '', ''),
(3, 'kk', '', '', '', ''),
(3, 'ru', 'Контакты', '', '', ''),
(4, 'en', '', '', '', ''),
(4, 'kk', '', '', '', ''),
(4, 'ru', 'Новости', '', '', ''),
(5, 'en', '', '', '', ''),
(5, 'kk', '', '', '', ''),
(5, 'ru', 'Оплата и доставка', '', '', ''),
(6, 'en', '', '', '', ''),
(6, 'kk', '', '', '', ''),
(6, 'ru', 'Жалобы и преложения', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1580184834),
('m130524_201442_init', 1580184836),
('m200121_113818_create_menu_table', 1580184839),
('m200121_113927_create_contact_table', 1580184841),
('m200121_114051_create_suggestion_table', 1580184843),
('m200121_114118_create_news_table', 1580207825),
('m200121_114149_create_delivery_table', 1580184847),
('m200121_115424_create_logo_table', 1580184847),
('m200121_115441_create_banner_table', 1580184848),
('m200124_113103_create_client_table', 1581067610),
('m200128_040548_create_language_table', 1580184849),
('m200128_041002_create_i18n_init_table', 1580184904),
('m200211_070332_create_requisites_table', 1581404929),
('m200211_084158_create_about_table', 1581411745),
('m200211_084220_create_about_item_table', 1581411746),
('m200211_084250_create_about_partners_table', 1581411746),
('m200212_091854_create_brand_table', 1581500278),
('m200212_093519_create_city_table', 1581500279),
('m200213_073806_create_guarantee_table', 1581580136),
('m200217_062231_create_catalog_table', 1581921780),
('m200217_075637_create_product_table', 1581929045),
('m200217_105400_create_product_description_table', 1581937256),
('m200217_105447_create_product_specifications_table', 1581937258),
('m200217_110429_create_product_downloads_table', 1581940533),
('m200220_041610_create_filter_attr_table', 1582176183),
('m200220_041627_create_filter_value_table', 1582176185),
('m200220_041642_create_filter_entity_table', 1582176187);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `status`, `title`, `slug`, `description`, `content`, `image`, `metaName`, `metaDesc`, `metaKey`, `date`) VALUES
(1, 1, 'Купите два маршрутизатора и получите 3-й <br> в подарок', 'kupite-dva-marshrutizatora-i-poluchite-3-y-v-podarok', '<p>Акция действует с 28 ноября 2019г <span style=\"color:rgb(232, 191, 106)\">&lt;br&gt; </span>по 30 ноября 2019г</p>\r\n', '<p>Consequat mollit officia ullamco deserunt consequat eu exercitation voluptate. Ullamco duis dolore in non deserunt quis adipisicing esse laboris aliqua. Dolor qui esse voluptate proident cupidatat Lorem amet reprehenderit sint ullamco qui aliqua. Qui voluptate cillum laborum nisi enim veniam cillum enim sit. Occaecat qui pariatur aliquip voluptate tempor ullamco occaecat sunt nostrud. Ipsum quis consectetur minim eiusmod laboris esse.</p>\r\n', '1581397995_news.png', '', '', '', '2020-02-12');

-- --------------------------------------------------------

--
-- Структура таблицы `news_translation`
--

CREATE TABLE `news_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `news_translation`
--

INSERT INTO `news_translation` (`id`, `language`, `title`, `description`, `content`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'en', '', '', '', '', '', ''),
(1, 'kk', '', '', '', '', '', ''),
(1, 'ru', 'Купите два маршрутизатора и получите 3-й <br> в подарок', '<p>Акция действует с 28 ноября 2019г <span style=\"color:rgb(232, 191, 106)\">&lt;br&gt; </span>по 30 ноября 2019г</p>\r\n', '<p>Consequat mollit officia ullamco deserunt consequat eu exercitation voluptate. Ullamco duis dolore in non deserunt quis adipisicing esse laboris aliqua. Dolor qui esse voluptate proident cupidatat Lorem amet reprehenderit sint ullamco qui aliqua. Qui voluptate cillum laborum nisi enim veniam cillum enim sit. Occaecat qui pariatur aliquip voluptate tempor ullamco occaecat sunt nostrud. Ipsum quis consectetur minim eiusmod laboris esse.</p>\r\n', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_partner` int(11) DEFAULT NULL,
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci,
  `create_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `category_id`, `status`, `name`, `code`, `price`, `price_partner`, `slug`, `content`, `image`, `sort`, `metaName`, `metaDesc`, `metaKey`, `create_at`) VALUES
(1, 19, 1, 'АККУМУЛЯТОР POWERPLANT', 'px-6012', 2000, 1500, 'akkumulyator-powerplant', '', '1582018617_товар5.png', 20, '', '', '', NULL),
(2, 15, 1, 'НОУТБУК DREAM MACHINES', '321212', 78000, 65000, 'noutbuk-dream-machines', '', '', 20, '', '', '', NULL),
(3, 12, 1, 'N218WP', '4443223', 5000, 4500, 'n218wp', '', '1582018777_товар3.png', 20, '', '', '', NULL),
(4, 7, 1, 'Маршрутизатор D-Link DIR-615A/A1A беспроводной', 'ar12221', 7800, 7000, 'marshrutizator-d-link-dir-615aa1a-besprovodnoy', '', '1582018876_router2.png', 20, '', '', '', NULL),
(5, 17, 1, 'Маршрутизатор D-Link DIR беспроводной', 'tr-1231221312', 5000, 4000, 'marshrutizator-d-link-dir-besprovodnoy', '', '1582018957_n-in-img.png', 20, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `product_description`
--

CREATE TABLE `product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_description_translation`
--

CREATE TABLE `product_description_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_downloads`
--

CREATE TABLE `product_downloads` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_downloads_translation`
--

CREATE TABLE `product_downloads_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_specifications`
--

CREATE TABLE `product_specifications` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_specifications_translation`
--

CREATE TABLE `product_specifications_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `product_translation`
--

CREATE TABLE `product_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `metaName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metaDesc` text COLLATE utf8_unicode_ci,
  `metaKey` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `product_translation`
--

INSERT INTO `product_translation` (`id`, `language`, `name`, `content`, `metaName`, `metaDesc`, `metaKey`) VALUES
(1, 'en', '', '', '', '', ''),
(1, 'kk', 'Казахский аккумулятор', '', '', '', ''),
(1, 'ru', 'АККУМУЛЯТОР POWERPLANT', '', '', '', ''),
(2, 'en', '', '', '', '', ''),
(2, 'kk', '', '', '', '', ''),
(2, 'ru', 'НОУТБУК DREAM MACHINES', '', '', '', ''),
(3, 'en', '', '', '', '', ''),
(3, 'kk', '', '', '', '', ''),
(3, 'ru', 'N218WP', '', '', '', ''),
(4, 'en', '', '', '', '', ''),
(4, 'kk', '', '', '', '', ''),
(4, 'ru', 'Маршрутизатор D-Link DIR-615A/A1A беспроводной', '', '', '', ''),
(5, 'en', '', '', '', '', ''),
(5, 'kk', '', '', '', '', ''),
(5, 'ru', 'Маршрутизатор D-Link DIR беспроводной', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `requisites`
--

CREATE TABLE `requisites` (
  `id` int(11) NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `requisites`
--

INSERT INTO `requisites` (`id`, `source`, `value`) VALUES
(1, 'Наименование организации:', 'ТОО \"National Security & Communication\"'),
(2, 'Юридический адрес:', '050000, Республика Казахстан, г. Алматы, пр. Райымбека 162'),
(3, 'БИН:', '160340027955'),
(4, 'Расчетный счет в KZT:', 'KZ039470398991290510'),
(5, 'Банк:', 'АО БД «Альфа-Банк»'),
(6, 'БИК:', 'ALFAKZKA'),
(7, 'Первый руководитель:', 'Директор Ахунбаев <br>Руслан Тахирович');

-- --------------------------------------------------------

--
-- Структура таблицы `requisites_translation`
--

CREATE TABLE `requisites_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `requisites_translation`
--

INSERT INTO `requisites_translation` (`id`, `language`, `source`, `value`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Наименование организации:', 'ТОО \"National Security & Communication\"'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Юридический адрес:', '050000, Республика Казахстан, г. Алматы, пр. Райымбека 162'),
(3, 'en', '', ''),
(3, 'kk', '', ''),
(3, 'ru', 'БИН:', '160340027955'),
(4, 'en', '', ''),
(4, 'kk', '', ''),
(4, 'ru', 'Расчетный счет в KZT:', 'KZ039470398991290510'),
(5, 'en', '', ''),
(5, 'kk', '', ''),
(5, 'ru', 'Банк:', 'АО БД «Альфа-Банк»'),
(6, 'en', '', ''),
(6, 'kk', '', ''),
(6, 'ru', 'БИК:', 'ALFAKZKA'),
(7, 'en', '', ''),
(7, 'kk', '', ''),
(7, 'ru', 'Первый руководитель:', 'Директор Ахунбаев <br>Руслан Тахирович');

-- --------------------------------------------------------

--
-- Структура таблицы `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `suggestion`
--

CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `suggestion`
--

INSERT INTO `suggestion` (`id`, `title`, `content`) VALUES
(1, 'Уважаемый Покупатель!', '<p> Мы искренне рады, что Вы стали нашим Клиентом! <br> Мы ценим своих Клиентов и постоянно работаем\r\n                        над повышением уровня их обслуживания.</p>\r\n                    <p>Мы стараемся сделать так, чтобы все наши покупатели оставались довольны покупками и уровнем\r\n                        сервиса.</p>\r\n                    <p>Мы будем рады, если Вы сообщите нам о своих пожеланиях, предложениях или возможных претензиях,\r\n                        которые возникли у Вас в процессе взаимодействия с нашей компанией!</p>'),
(2, 'Сколько времени займет рассмотрение вопроса?', '<p>Мы постарались сделать процедуру подачи и рассмотрения предложений и жалоб максимально эффективной и быстрой. Наши специалисты внимательно изучат ваш вопрос и в кратчайшие сроки ответят вам.</p>\r\n\r\n<p>Жалобы и предложения, на которые вы желаете получить устный ответ (по телефону), рассматриваются в течение 3 (Трех) дней со дня их регистрации в регистре по предложениям и жалобам компании. Если есть такая возможность, и клиент согласен получить устный ответ, сотрудник Компании предоставляет такой ответ на предложение или жалобу клиента сразу.</p>\r\n\r\n<p>Процедура рассмотрения предложений и жалоб может занять до 30 (Тридцати) дней со дня их регистрации в регистре по предложениям и жалобам компании.</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `suggestion_translation`
--

CREATE TABLE `suggestion_translation` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `suggestion_translation`
--

INSERT INTO `suggestion_translation` (`id`, `language`, `title`, `content`) VALUES
(1, 'en', '', ''),
(1, 'kk', '', ''),
(1, 'ru', 'Уважаемый Покупатель!', '<p> Мы искренне рады, что Вы стали нашим Клиентом! <br> Мы ценим своих Клиентов и постоянно работаем\r\n                        над повышением уровня их обслуживания.</p>\r\n                    <p>Мы стараемся сделать так, чтобы все наши покупатели оставались довольны покупками и уровнем\r\n                        сервиса.</p>\r\n                    <p>Мы будем рады, если Вы сообщите нам о своих пожеланиях, предложениях или возможных претензиях,\r\n                        которые возникли у Вас в процессе взаимодействия с нашей компанией!</p>'),
(2, 'en', '', ''),
(2, 'kk', '', ''),
(2, 'ru', 'Сколько времени займет рассмотрение вопроса?', '<p>Мы постарались сделать процедуру подачи и рассмотрения предложений и жалоб максимально эффективной и быстрой. Наши специалисты внимательно изучат ваш вопрос и в кратчайшие сроки ответят вам.</p>\r\n\r\n<p>Жалобы и предложения, на которые вы желаете получить устный ответ (по телефону), рассматриваются в течение 3 (Трех) дней со дня их регистрации в регистре по предложениям и жалобам компании. Если есть такая возможность, и клиент согласен получить устный ответ, сотрудник Компании предоставляет такой ответ на предложение или жалобу клиента сразу.</p>\r\n\r\n<p>Процедура рассмотрения предложений и жалоб может занять до 30 (Тридцати) дней со дня их регистрации в регистре по предложениям и жалобам компании.</p>\r\n');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `auth_key`, `password_hash`, `token`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'developer@neor.info', 'Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v', '$2y$13$4CdvdT.vDNeudoxxnc.TVOWkaxD1wGwu.f1sZT4UBrI9HR1VZQ3t2', NULL, 32, 1, '2020-01-26 18:00:00', '2020-01-27 18:00:00'),
(2, 'manager@alux.kz', 'Jda-l5Tlc6w3_HUYQd0S__xgquoqkZ9v', '$2y$13$4CdvdT.vDNeudoxxnc.TVOWkaxD1wGwu.f1sZT4UBrI9HR1VZQ3t2', NULL, 8, 1, '2020-01-26 18:00:00', '2020-01-26 18:00:00'),
(11, 'kkokoneor@gmail.com', 'yWIGb24eBH4p--agYWFU-Q6qd4sXRdDk', '$2y$13$4CdvdT.vDNeudoxxnc.TVOWkaxD1wGwu.f1sZT4UBrI9HR1VZQ3t2', 'a6_fXF8MIjqDEplDtMWJ_WXMu9uqZpWg_1581327582', 1, 1, '2020-02-10 06:39:41', '2020-02-10 06:41:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_item`
--
ALTER TABLE `about_item`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_item_translation`
--
ALTER TABLE `about_item_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_about_item_translation_language` (`language`);

--
-- Индексы таблицы `about_partners`
--
ALTER TABLE `about_partners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `about_translation`
--
ALTER TABLE `about_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_about_translation_language` (`language`);

--
-- Индексы таблицы `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banner_translation`
--
ALTER TABLE `banner_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_banner_translation_language` (`language`);

--
-- Индексы таблицы `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brand_translation`
--
ALTER TABLE `brand_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_brand_translation_language` (`language`);

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `catalog_translation`
--
ALTER TABLE `catalog_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_catalog_translation_language` (`language`);

--
-- Индексы таблицы `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `city_translation`
--
ALTER TABLE `city_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_city_translation_language` (`language`);

--
-- Индексы таблицы `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_client_user` (`user_id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact_translation`
--
ALTER TABLE `contact_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_contact_translation_language` (`language`);

--
-- Индексы таблицы `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `delivery_translation`
--
ALTER TABLE `delivery_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_delivery_translation_language` (`language`);

--
-- Индексы таблицы `filter_attr`
--
ALTER TABLE `filter_attr`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `filter_attr_translation`
--
ALTER TABLE `filter_attr_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_filter_attr_translation_language` (`language`);

--
-- Индексы таблицы `filter_entity`
--
ALTER TABLE `filter_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_filter_entity_filter_value` (`value_id`),
  ADD KEY `fk_filter_entity_product` (`product_id`);

--
-- Индексы таблицы `filter_value`
--
ALTER TABLE `filter_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_filter_value_filter_attr` (`attribute_id`);

--
-- Индексы таблицы `filter_value_translation`
--
ALTER TABLE `filter_value_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_filter_value_translation_language` (`language`);

--
-- Индексы таблицы `guarantee`
--
ALTER TABLE `guarantee`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `guarantee_translation`
--
ALTER TABLE `guarantee_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_guarantee_translation_language` (`language`);

--
-- Индексы таблицы `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_menu_translation_language` (`language`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news_translation`
--
ALTER TABLE `news_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_news_translation_language` (`language`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_catalog` (`category_id`);

--
-- Индексы таблицы `product_description`
--
ALTER TABLE `product_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_description_product` (`product_id`);

--
-- Индексы таблицы `product_description_translation`
--
ALTER TABLE `product_description_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_product_description_translation_language` (`language`);

--
-- Индексы таблицы `product_downloads`
--
ALTER TABLE `product_downloads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_downloads_product` (`product_id`);

--
-- Индексы таблицы `product_downloads_translation`
--
ALTER TABLE `product_downloads_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_product_downloads_translation_language` (`language`);

--
-- Индексы таблицы `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_specifications_product` (`product_id`);

--
-- Индексы таблицы `product_specifications_translation`
--
ALTER TABLE `product_specifications_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_product_specifications_translation_language` (`language`);

--
-- Индексы таблицы `product_translation`
--
ALTER TABLE `product_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_product_translation_language` (`language`);

--
-- Индексы таблицы `requisites`
--
ALTER TABLE `requisites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `requisites_translation`
--
ALTER TABLE `requisites_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_requisites_translation_language` (`language`);

--
-- Индексы таблицы `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Индексы таблицы `suggestion`
--
ALTER TABLE `suggestion`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `suggestion_translation`
--
ALTER TABLE `suggestion_translation`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_suggestion_translation_language` (`language`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `token` (`token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `about_item`
--
ALTER TABLE `about_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `about_partners`
--
ALTER TABLE `about_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `filter_attr`
--
ALTER TABLE `filter_attr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `filter_entity`
--
ALTER TABLE `filter_entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `filter_value`
--
ALTER TABLE `filter_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT для таблицы `guarantee`
--
ALTER TABLE `guarantee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `logo`
--
ALTER TABLE `logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `product_description`
--
ALTER TABLE `product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `product_downloads`
--
ALTER TABLE `product_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `product_specifications`
--
ALTER TABLE `product_specifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `requisites`
--
ALTER TABLE `requisites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `suggestion`
--
ALTER TABLE `suggestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `about_item_translation`
--
ALTER TABLE `about_item_translation`
  ADD CONSTRAINT `fk_about_item_translation_about_item` FOREIGN KEY (`id`) REFERENCES `about_item` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `about_translation`
--
ALTER TABLE `about_translation`
  ADD CONSTRAINT `fk_about_translation_about` FOREIGN KEY (`id`) REFERENCES `about` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `banner_translation`
--
ALTER TABLE `banner_translation`
  ADD CONSTRAINT `fk_banner_translation_banner` FOREIGN KEY (`id`) REFERENCES `banner` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `brand_translation`
--
ALTER TABLE `brand_translation`
  ADD CONSTRAINT `fk_brand_translation_brand` FOREIGN KEY (`id`) REFERENCES `brand` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_translation`
--
ALTER TABLE `catalog_translation`
  ADD CONSTRAINT `fk_catalog_translation_catalog` FOREIGN KEY (`id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `city_translation`
--
ALTER TABLE `city_translation`
  ADD CONSTRAINT `fk_city_translation_city` FOREIGN KEY (`id`) REFERENCES `city` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `contact_translation`
--
ALTER TABLE `contact_translation`
  ADD CONSTRAINT `fk_contact_translation_contact` FOREIGN KEY (`id`) REFERENCES `contact` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `delivery_translation`
--
ALTER TABLE `delivery_translation`
  ADD CONSTRAINT `fk_delivery_translation_delivery` FOREIGN KEY (`id`) REFERENCES `delivery` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_attr_translation`
--
ALTER TABLE `filter_attr_translation`
  ADD CONSTRAINT `fk_filter_attr_translation_filter_attr` FOREIGN KEY (`id`) REFERENCES `filter_attr` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_entity`
--
ALTER TABLE `filter_entity`
  ADD CONSTRAINT `fk_filter_entity_filter_value` FOREIGN KEY (`value_id`) REFERENCES `filter_value` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_filter_entity_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_value`
--
ALTER TABLE `filter_value`
  ADD CONSTRAINT `fk_filter_value_filter_attr` FOREIGN KEY (`attribute_id`) REFERENCES `filter_attr` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `filter_value_translation`
--
ALTER TABLE `filter_value_translation`
  ADD CONSTRAINT `fk_filter_value_translation_filter_value` FOREIGN KEY (`id`) REFERENCES `filter_value` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `guarantee_translation`
--
ALTER TABLE `guarantee_translation`
  ADD CONSTRAINT `fk_guarantee_translation_guarantee` FOREIGN KEY (`id`) REFERENCES `guarantee` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_translation`
--
ALTER TABLE `menu_translation`
  ADD CONSTRAINT `fk_menu_translation_menu` FOREIGN KEY (`id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `news_translation`
--
ALTER TABLE `news_translation`
  ADD CONSTRAINT `fk_news_translation_news` FOREIGN KEY (`id`) REFERENCES `news` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_catalog` FOREIGN KEY (`category_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_description`
--
ALTER TABLE `product_description`
  ADD CONSTRAINT `fk_product_description_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_description_translation`
--
ALTER TABLE `product_description_translation`
  ADD CONSTRAINT `fk_product_description_translation_product_description` FOREIGN KEY (`id`) REFERENCES `product_description` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_downloads`
--
ALTER TABLE `product_downloads`
  ADD CONSTRAINT `fk_product_downloads_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_downloads_translation`
--
ALTER TABLE `product_downloads_translation`
  ADD CONSTRAINT `fk_product_downloads_translation_product_downloads` FOREIGN KEY (`id`) REFERENCES `product_downloads` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD CONSTRAINT `fk_product_specifications_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_specifications_translation`
--
ALTER TABLE `product_specifications_translation`
  ADD CONSTRAINT `fk_product_specifications_translation_product_specifications` FOREIGN KEY (`id`) REFERENCES `product_specifications` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_translation`
--
ALTER TABLE `product_translation`
  ADD CONSTRAINT `fk_product_translation_product` FOREIGN KEY (`id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `requisites_translation`
--
ALTER TABLE `requisites_translation`
  ADD CONSTRAINT `fk_requisites_translation_requisites` FOREIGN KEY (`id`) REFERENCES `requisites` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `suggestion_translation`
--
ALTER TABLE `suggestion_translation`
  ADD CONSTRAINT `fk_suggestion_translation_suggestion` FOREIGN KEY (`id`) REFERENCES `suggestion` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
