<?php
namespace common\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string  $email
 * @property string  $auth_key
 * @property string  $password_hash
 * @property string  $token
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \common\modules\user\models\User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['user_id' => 'id']);
    }

    /**
     *
     */
    public function getUsername()
    {
        return (!empty($this->client)) ? $this->client->username : '';
    }

    public static function getOne()
    {
        $model  = self::findOne(['id' => Yii::$app->user->identity->id, 'status' => self::STATUS_ACTIVE]);

        return $model;
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id']);
    }

    public static function userClient()
    {
        $model  = static::findOne(['id' => Yii::$app->user->identity->id]);
        $userAddress    = UserAddress::find()->where(['user_id' => $model->id])->orderBy(['id' => SORT_ASC])->one();

        $user   = [
            'id'            => $model->id,
            'token'         => $model->token,
            'username'      => $model->client->username,
            'phone'         => $model->phone,
            'email'         => $model->email,
            'birthday'      => $model->client->birthday,
            'address'       => $userAddress ? $userAddress->address : '',
            'apartment'     => $userAddress ? $userAddress->apartment : '',
            'storey'        => $userAddress ? $userAddress->storey : '',
            'porch'         => $userAddress ? $userAddress->porch : '',
            'intercom'      => $userAddress ? $userAddress->intercom : '',
        ];

        return $user;
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'email');
    }
}
