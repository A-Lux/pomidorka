<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $iframe
 * @property string|null $play_market
 * @property string|null $app_store
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address', 'email', 'iframe', 'play_market', 'app_store'], 'string'],
            [['phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'address'       => 'Адрес',
            'phone'         => 'Телефон',
            'email'         => 'Email',
            'iframe'        => 'Карта',
            'play_market'   => 'Play Market',
            'app_store'     => 'App Store',
        ];
    }

    public static function getOne()
    {
        return self::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
