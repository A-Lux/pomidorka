<?php

namespace backend\models;

use common\models\Client;
use common\models\User;
use yii\base\Model;

class ClientConfirmed extends Model
{
    public static function isConfirmed($model)
    {
        $client     = Client::findOne(['id' => $model->id]);
        $user       = User::findOne(['id' => $client->user_id]);

        $client->status           = Client::STATUS_PARTNER;
        $client->is_confirmed     = Client::IS_CONFIRMED_PARTNER;

        if($client->save()){
            self::sendIsConfirmedPartner($user->email);
            return true;
        }

        return false;
    }

    public static function notConfirmed($model)
    {
        $client     = Client::findOne(['id' => $model->id]);
        $user       = User::findOne(['id' => $client->user_id]);

        $client->status           = Client::STATUS_USER;
        $client->is_confirmed     = Client::NOT_CONFIRMED_PARTNER;

        if($client->save()){
            self::sendNotConfirmedPartner($user->email);
            return true;
        }

        return false;
    }

    protected static function sendIsConfirmedPartner($email)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «NSCOM»'])
            ->setTo($email)
            ->setSubject('Подтверждение статуса партнера')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на Вы получили статус партнера на сайте <a href='$host'>«NSCOM»</a>.</p> </br>
                                 </br>
                                 <p>Вы сможете получить информации о товарах и скидках!</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«NSCOM»</a></p>");

        return $emailSend->send();

    }

    protected static function sendNotConfirmedPartner($email)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ТОО «NSCOM»'])
            ->setTo($email)
            ->setSubject('Подтверждение статуса партнера')
            ->setHtmlBody("<p>Вам отказали в статусе партнера на сайте <a href='$host'>«NSCOM»</a>.</p> </br>
                                 </br>
                                 <p>К сожалению, Вы не подходите по определенным причинам для партнера!</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«NSCOM»</a></p>");

        return $emailSend->send();

    }
}