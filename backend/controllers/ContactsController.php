<?php

namespace backend\controllers;

use Yii;
use common\models\Contacts;
use backend\models\search\ContactsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactsController implements the CRUD actions for Contacts model.
 */
class ContactsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createTranslation',
        'view'   => 'viewTranslation',
        'update' => 'updateTranslation',
        'index'  => 'indexTranslation',
        'delete' => 'deleteTranslation',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Contacts::className();
            $this->searchModel = ContactsSearch::className();

            return true;
        }

        return false;
    }
}
