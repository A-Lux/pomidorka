<?php

namespace backend\controllers;

use Yii;
use common\models\Menu;
use backend\models\search\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createMenu',
        'view'   => 'viewMenu',
        'update' => 'updateMenu',
        'index'  => 'indexMenu',
        'delete' => 'deleteMenu',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Menu::className();
            $this->searchModel = MenuSearch::className();

            return true;
        }

        return false;
    }
}
