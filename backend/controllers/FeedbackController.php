<?php

namespace backend\controllers;

use Yii;
use common\models\Feedback;
use backend\models\search\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Feedback::className();
            $this->searchModel = FeedbackSearch::className();

            return true;
        }

        return false;
    }
}
