<?php

namespace backend\controllers;

use Yii;
use common\models\SourceMessage;
use backend\models\search\SourceMessageSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\BackendController;

/**
 * SourceMessageController implements the CRUD actions for SourceMessage model.
 */
class SourceMessageController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createTranslation',
        'view'   => 'viewTranslation',
        'update' => 'updateTranslation',
        'index'  => 'indexTranslation',
        'delete' => 'deleteTranslation',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = SourceMessage::className();
            $this->searchModel = SourceMessageSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Updates an existing SourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        /* @var $model SourceMessage  */
        $model = $this->findModel($id);

        $model->initMessages();

        if(Model::loadMultiple($model->messages, Yii::$app->getRequest()->post()) && Model::validateMultiple($model->messages)){
            $model->saveMessage();

            \Yii::$app->session->setFlash('info', 'Изменения приняты!');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Указанная страница не найдена.');
    }
}
