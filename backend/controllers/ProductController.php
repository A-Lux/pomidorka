<?php

namespace backend\controllers;

use common\models\ProductDetails;
use Yii;
use common\models\Product;
use backend\models\search\ProductSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createProduct',
        'view'   => 'viewProduct',
        'update' => 'updateProduct',
        'index'  => 'indexProduct',
        'delete' => 'deleteProduct',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Product::className();
            $this->searchModel = ProductSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model      = new Product();

        if ($model->load(Yii::$app->request->post())) {

            $this->createImage($model);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));

            if ($model->save()) {
                $this->saveDetails(Yii::$app->request->post()['details'], $model);
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model      = $this->findModel($id);
        $details    = $this->productDetails($model);

        $oldImage   = $model->image;

        if ($model->load(Yii::$app->request->post())) {

//            print_r("<pre>");
//            print_r(Yii::$app->request->post()['details']); die;
            $this->updateImage($model, $oldImage);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));

            if ($model->save()) {

                $this->saveDetails(Yii::$app->request->post()['details'], $model);

                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model'                 => $model,
            'details'               => $details,
        ]);
    }

    public function saveDetails($json, $model)
    {
        ProductDetails::deleteAll(['product_id' => $model->id]);
        $array    = Json::decode($json, true);

        foreach ($array as $item){
            $detail                 = new ProductDetails();
            $detail->product_id     = $model->id;
            $detail->type_id        = !empty($item['type']) ? $item['type'] : null;
            $detail->attribute_id   = !empty($item['attribute']) ? $item['attribute'] : null;
            $detail->price          = !empty($item['price']) ? $item['price'] : null;
            $detail->save();
        }

        return $array;
    }

    private function productDetails($model)
    {
        $details    = ProductDetails::findAll(['product_id' => $model->id]);
        $array      = [];

        foreach ($details as $detail){
            $array[]    = [
                'id'        => $detail->id,
                'type'      => $detail->type_id,
                'attribute' => $detail->attribute_id,
                'price'     => $detail->price,
            ];
        }

        return Json::encode($array);
    }
}
