<?php

namespace backend\controllers;

use Yii;
use common\models\Faq;
use backend\models\search\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Faq::className();
            $this->searchModel = FaqSearch::className();

            return true;
        }

        return false;
    }
}
