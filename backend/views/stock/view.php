<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Stock;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Stock */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Акции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="stock-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'filter' => Stock::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Stock::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            'slug:ntext',
            'title',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'create_at',
        ],
    ]) ?>

</div>
