<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Faq;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FaqSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Часто задаваемые вопросы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'format' => 'raw',
                'attribute' => 'question',
                'value' => function($model){
                    return $model->question;
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'answer',
                'value' => function($model){
                    return $model->answer;
                }
            ],
            [
                'attribute' => 'status',
                'filter' => Faq::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Faq::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
//            'sort',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
