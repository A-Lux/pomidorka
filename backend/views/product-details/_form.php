<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Product;
use common\models\Types;
use common\models\Attributes;

/* @var $this yii\web\View */
/* @var $model common\models\ProductDetails */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-details-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(Product::getList(), [
        'prompt' => 'Выберите продукт'
    ]) ?>

    <?= $form->field($model, 'type_id')->dropDownList(Types::getList(), [
        'prompt' => 'Выберите тип'
    ]) ?>

    <?= $form->field($model, 'attribute_id')->dropDownList(Attributes::getList(), [
        'prompt' => 'Выберите аттрибут'
    ]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
