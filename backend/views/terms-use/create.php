<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TermsUse */

$this->title = 'Создание Пользовательское соглашение';
$this->params['breadcrumbs'][] = ['label' => 'Пользовательское соглашение', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terms-use-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
