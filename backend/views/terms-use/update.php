<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TermsUse */

$this->title = 'Редактировать Пользовательское соглашение: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Пользовательское соглашение', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="terms-use-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
