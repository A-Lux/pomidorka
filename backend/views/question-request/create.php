<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\QuestionRequest */

$this->title = 'Создание Question Request';
$this->params['breadcrumbs'][] = ['label' => 'Question Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
