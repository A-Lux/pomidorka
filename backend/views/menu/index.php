<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Menu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'key',
            'name',
            'url:url',
            [
                'attribute' => 'status',
                'filter' => Menu::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Menu::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'position',
                'filter' => Menu::positionDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Menu::positionDescription(), $model->position);
                },
                'format' => 'raw',
            ],
            //'position',
            //'type',
            //'content:ntext',
            //'sort',
            //'metaName',
            //'metaDesc:ntext',
            //'metaKey:ntext',
            //'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
