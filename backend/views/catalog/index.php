<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\Catalog;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CatalogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'parent_id',
            [
                'attribute' => 'status',
                'filter' => Catalog::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Catalog::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            [
                'attribute' => 'main',
                'filter' => Catalog::mainStatusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Catalog::mainStatusDescription(), $model->main);
                },
                'format' => 'raw',
            ],
            [
                'value' => function ($model) {
                    return
                        Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                        Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align: center'],
            ],
//            'slug',
            //'content:ntext',
            //'image',
            //'level',
            //'sort',
            //'metaName',
            //'metaDesc:ntext',
            //'metaKey:ntext',
            'create_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
