<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
/**
 * Do not use this code in your template. Remove it.
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <?php $this->head() ?>
        <link rel="icon" type="image/png" href="/images/favicon.png">
    </head>
    <body class="<?= \dmstr\helpers\AdminLteHelper::skinClass() ?>">
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            '_header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <?= $this->render(
            '_left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>

        <?= \common\widgets\ToastrAlert::widget(); ?>
        <?= $this->render(
            '_content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.products-banner').select2();
            $('.js-example-theme-multiple').select2({
                placeholder: 'Выберите значение фильтра',
                width: '100%'
            });
        })
        document.getElementsByClassName('selectedBtn')[0].addEventListener('submit', function() {
            if (document.querySelectorAll('.select2-selection__placeholder')) {
            }
        })
    </script>
    
    </html>
    <?php $this->endPage() ?>
<?php } ?>
