<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],

                    ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/menu']],
                    ['label' => 'Каталог', 'icon' => 'bookmark-o', 'url' => ['/catalog']],
                    [
                        'label' => 'Продукция',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Продукт', 'icon' => 'ship', 'url' => ['/product']],
                            ['label' => 'Типы продуктов', 'icon' => 'bookmark-o', 'url' => ['/types']],
                            ['label' => 'Аттрибуты продукта', 'icon' => 'bookmark-o', 'url' => ['/attributes']],
                            ['label' => 'Описание товара', 'icon' => 'bookmark-o', 'url' => ['/product-details']],
                        ],
                    ],

                    ['label' => 'О нас', 'icon' => 'ship', 'url' => ['/about']],
                    ['label' => 'Акция', 'icon' => 'bookmark-o', 'url' => ['/stock']],
                    ['label' => 'Контакты', 'icon' => 'bookmark-o', 'url' => ['/contacts']],

                    ['label' => 'Доставка', 'icon' => 'info', 'url' => ['/delivery'],],
                    ['label' => 'Политика конфиденциальности', 'icon' => 'info', 'url' => ['/privacy-policy'],],
                    ['label' => 'Пользовательское соглашение', 'icon' => 'info', 'url' => ['/terms-use'],],
                    ['label' => 'Часто задаваемые вопросы', 'icon' => 'info', 'url' => ['/faq'],],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
//                    ['label' => 'Информация о клиентах', 'icon' => 'user-circle-o', 'url' => ['/client']],
                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banner']],
                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
                    ['label' => 'Социальные сети', 'icon' => 'image', 'url' => ['/social-networks']],
                    ['label' => 'Обратная связь', 'icon' => 'ticket', 'url' => ['/feedback']],
                    ['label' => 'Город', 'icon' => 'map', 'url' => ['/cities']],
                    ['label' => 'Адреса', 'icon' => 'map', 'url' => ['/address']],
//                    ['label' => 'Элекронная почта', 'icon' => 'ship', 'url' => ['/email-for-request']],

                ],
            ]
        ) ?>

    </section>

</aside>
