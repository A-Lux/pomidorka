<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <div class="jumbotron">
        <h1><?= Html::encode($this->title) ?></h1>
        <?= nl2br(Html::encode($message)) ?>
    </div>

</div>
